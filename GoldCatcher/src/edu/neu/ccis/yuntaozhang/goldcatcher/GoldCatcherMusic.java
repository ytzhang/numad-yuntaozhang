package edu.neu.ccis.yuntaozhang.goldcatcher;

import android.content.Context;
import android.media.MediaPlayer;
import edu.neu.ccis.yuntaozhang.goldcatcher.GCShared;
import edu.neu.ccis.yuntaozhang.R;

public class GoldCatcherMusic {
	private static MediaPlayer mp = null;
	private static MediaPlayer mp1 = null;
	private static MediaPlayer mp2 = null;

	/** Stop old song and start new one */

	public static void playGoldCatcher(Context context, int resource) {
		stop(context);
		if(GCShared.music) {
			mp = MediaPlayer.create(context, resource);
			mp.setLooping(true);
			mp.start();
		}
	}

	public static void playOnce(Context context, int resource) {
		if(GCShared.music){
			if(mp1 != null){
				mp1.release();
				mp1 = null;
			}
			mp1 = MediaPlayer.create(context, resource);
			mp1.start();
		}
	}

	public static void outOfTime(Context context){
		if(GCShared.music){
			if(mp2 != null){
				mp2.release();
				mp2 = null;
			}
			mp2 = MediaPlayer.create(context, R.raw.outoftime);
			mp2.start();
		}
	}
	
	public static void stopOutOfTime(Context context){
		if(mp2 != null){
			mp2.stop();
			mp2.release();
			mp2 = null;
		}
	}


	/** Stop the music */
	public static void stop(Context context) { 
		if (mp != null) {
			mp.stop();
			mp.release();
			mp = null;
		}
	}
}