package edu.neu.ccis.yuntaozhang.goldcatcher;

import edu.neu.ccis.yuntaozhang.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

public class GoldCatcher extends Activity implements OnClickListener {

	public static final String INFO = "Game Info";
	public static final int SCORE = 0;
	public static final int TIME = 1;
	public static final int TOTAL_TIME = 60;
	public static final int RED_LIGHT = 2;
	public static final int GOLD = 3;
	public static final int ROCK = 4;
	public static final String BEST_SCORE = "Personal Best Score";
	public static final String PLAYING = "Ongoing game";
	public static final String BACK = "Come back to game";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gold_catcher_main);
		
		//PhoneCheckAPI.doAuthorization(this);
		
		View continueGame = findViewById(R.id.gc_continue_button);
		View newGame = findViewById(R.id.gc_new_button);
		if(GCShared.onGame){
			continueGame.setOnClickListener(this);
			continueGame.setVisibility(View.VISIBLE);
		}else{
			continueGame.setVisibility(View.GONE);
			
		}
		
		newGame.setOnClickListener(this);
		View about = findViewById(R.id.gc_about_button);
		about.setOnClickListener(this);
		View acknowledgements = findViewById(R.id.gc_acknowledgements_button);
		acknowledgements.setOnClickListener(this);
		View settings = findViewById(R.id.gc_settings_button);
		settings.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gold_catcher_main, menu);
		return true;
	}


	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.gc_continue_button:
			Intent resume = new Intent(this, GoldCatcherGame.class);
			resume.putExtra(BACK, true);
			startActivity(resume);
			break;
		case R.id.gc_about_button:
			Intent about = new Intent(this, GoldCatcherAbout.class);
			startActivity(about);
			break;
		case R.id.gc_acknowledgements_button:
			Intent ack = new Intent(this, GCAcknowledgements.class);
			startActivity(ack);
			break;
		case R.id.gc_new_button:
			Intent start = new Intent(this, GoldCatcherGame.class);
			start.putExtra(BACK, false);
			startActivity(start);
			break;
		case R.id.gc_settings_button:
			Intent settings = new Intent(this, GoldCatcherSettings.class);
			startActivity(settings);
			break;
		}

	}

}
