package edu.neu.ccis.yuntaozhang.goldcatcher;

import edu.neu.ccis.yuntaozhang.R;
import edu.neu.ccis.yuntaozhang.R.layout;
import edu.neu.ccis.yuntaozhang.R.menu;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class GoldCatcherScoreBoard extends Activity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gold_catcher_score_board);
		int[] info = getIntent().getIntArrayExtra(GoldCatcher.INFO);
		TextView score = (TextView) findViewById(R.id.gc_game_score);
		TextView time = (TextView) findViewById(R.id.gc_workout_time);
		TextView gold = (TextView) findViewById(R.id.gc_num_golds);
		TextView rock = (TextView) findViewById(R.id.gc_num_rocks);
		TextView highest = (TextView) findViewById(R.id.gc_highest_score);
		score.setText("Score: " + info[GoldCatcher.SCORE]);
		time.setText(info[GoldCatcher.TIME]+"s");
		gold.setText(info[GoldCatcher.GOLD]+"");
		rock.setText(info[GoldCatcher.ROCK]+"");
		
		View restart = findViewById(R.id.gc_restart);
		View back = findViewById(R.id.gc_main_menu);
		restart.setOnClickListener(this);
		back.setOnClickListener(this);
		
		int bestScore = this.getSharedPreferences(GoldCatcher.INFO, MODE_PRIVATE).
				getInt(GoldCatcher.BEST_SCORE, 0);
		highest.setText(Math.max(bestScore,info[GoldCatcher.SCORE])+"");
		if(info[GoldCatcher.SCORE]>bestScore){
			findViewById(R.id.gc_new_high_score).setVisibility(View.VISIBLE);
			this.getSharedPreferences(GoldCatcher.INFO, MODE_PRIVATE).edit()
			.putInt(GoldCatcher.BEST_SCORE,info[GoldCatcher.SCORE]).commit();
		}
		GCShared.onGame = false;
		GoldCatcherMusic.playOnce(getBaseContext(), R.raw.stageclear);
			
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gold_catcher_score_board, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.gc_main_menu:
			GoldCatcherMusic.stopOutOfTime(getBaseContext());
			Intent main = new Intent(this, GoldCatcher.class);
			startActivity(main);
			break;
		case R.id.gc_restart:
			GoldCatcherMusic.stopOutOfTime(getBaseContext());
			Intent restart = new Intent(this, GoldCatcherGame.class);
			startActivity(restart);
			break;
		}
		
	}

}
