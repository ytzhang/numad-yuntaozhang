package edu.neu.ccis.yuntaozhang.goldcatcher;

public class GCShared {
    public static boolean onGame = false;
    public static boolean tutorial = true;
    public static final int SLIDE = 0;
    public static final int BUTTON = 1;
    public static final int TILT = 3;
    public static int control = SLIDE;
    public static boolean music = true;
    public static final int LEVEL_EASY = 0;
    public static final int LEVEL_MEDIUM = 1;
    public static final int LEVEL_HARD = 2;
    public static int level = LEVEL_MEDIUM;
}
