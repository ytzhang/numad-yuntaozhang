package edu.neu.ccis.yuntaozhang.goldcatcher;

import edu.neu.ccis.yuntaozhang.R;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class GoldCatcherGame extends Activity {
    @SuppressWarnings("unused")
    private final static String TAG = "GoldCathcerGame";

    private GoldCatcherGameView gcGameView;

    public static final String TIME = "Current time left";
    public static final String SCORE = "Current Score";
    public static final String WORKOUT = "Workout time so far";
    public static final String RED_LIGHT = "Red light last time";
    public static final String GOLD = "Total gold caught";
    public static final String ROCK = "Total rocks caught";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // get a instance of the game view
        gcGameView = new GoldCatcherGameView(this);

        // turn the boolean to indicate that the game has started
        GCShared.onGame = true;

        setContentView(gcGameView);
    }

    @Override
    protected void onResume() {
        super.onResume();

        gcGameView.timerStart();
        gcGameView.onResume();

        //start sensing
        gcGameView.startSimulation();
    }

    @Override
    protected void onPause() {
        super.onPause();

        gcGameView.timerStop();
        gcGameView.onPause();

        //stop sensing
        gcGameView.stopSimulation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.gold_catcher_game, menu);
        return true;
    }

}
