package edu.neu.ccis.yuntaozhang.hangoutaa;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class NewBill extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hangoutaa_newbill);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.new_bill, menu);
        return true;
    }

}
