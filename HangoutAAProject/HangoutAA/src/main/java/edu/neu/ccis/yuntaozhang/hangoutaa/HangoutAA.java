package edu.neu.ccis.yuntaozhang.hangoutaa;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class HangoutAA extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hangoutaa_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.hangout_a, menu);
        return true;
    }

    public void newBill(View view) {
        Intent newBill = new Intent(this, NewBill.class);
        startActivity(newBill);
    }

    public void settleDebts(View view) {
        Intent settleDebts = new Intent(this, Debts.class);
        startActivity(settleDebts);
    }

}
