//First Android app. Yuntao Zhang

package edu.neu.madcourse.yuntaozhang;

import android.telephony.TelephonyManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.widget.TextView;

public class AboutMe extends Activity {
	private String MyDeviceId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		
		MyDeviceId = tm.getDeviceId();
		
		setContentView(R.layout.main_about_me);
		
		//add android device id
		TextView textView = (TextView)findViewById(R.id.device_id);
		textView.setText(" IMEI:  "+MyDeviceId);	
	}
	
}