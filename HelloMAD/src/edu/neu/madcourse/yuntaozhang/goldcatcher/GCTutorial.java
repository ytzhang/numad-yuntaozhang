package edu.neu.madcourse.yuntaozhang.goldcatcher;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import edu.neu.madcourse.yuntaozhang.R;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;

public class GCTutorial extends View implements SensorEventListener{
	private final static String TAG = "GC_GameView";

	private final GoldCatcherGame goldCatcherGame;
	private SensorManager gcSensorManager;
	//private SensorManagerSimulator gcSensorManager;

	private int[] gameInfo = new int[5];
	private final static int itemsPerSec = 2;
	private WindowManager gcWindowManager;
	private Display gcDisplay;
	private Sensor gcAccelerometer;
	private Bitmap gcTree;
	private Bitmap catcherOpen;
	private Bitmap catcherClose;
	private Bitmap goldImg;
	private Bitmap rockImg;
	private Bitmap backButton;
	private Bitmap pauseButton;
	private Bitmap resumeButton;
	private Bitmap backToMainButton;
	private int wTree = 0;
	private int hTree = 0;
	private ItemBag items = new ItemBag();
	private Catcher catcher = new Catcher();;
	private float gcSensorX;
	private float gcSensorY;
	private boolean buttonLeft = false;
	private boolean buttonRight = false;
	private float[] gravity = {0,0,0};
	private boolean gameInProgress = false;
	private boolean paused = false;
	private boolean shaking = true;
	private final static int LAND_IN = 5000; // in millisecond
	private final static int TICK_RATE = 1; // in millisecond
	private float MOVE_RATE;
	private int time = GoldCatcher.TOTAL_TIME;
	private int score = 0;
	private int lightColor = Color.YELLOW;
	private final static String POLICE = "PoliceTimer";
	private Timer policeTimer = new Timer(POLICE);
	private Handler policeHandler = new Handler();
	private Runnable policeAct = new Runnable(){
		@Override
		public void run() {
			//Log.d(TAG, "Police timer running.");
			if(time <= 0) {
				////////////////////////////////////////////////////
				// cancel timer and end the game when time is up.
				////////////////////////////////////////////////////
				policeTimer.cancel();
				policeTimer.purge();
				gameInProgress = false;
				gameInfo[GoldCatcher.SCORE]= score;
				Intent timeup = new Intent(goldCatcherGame,GoldCatcherScoreBoard.class);
				timeup.putExtra(GoldCatcher.INFO, gameInfo);
				goldCatcherGame.startActivity(timeup);
				return;
			}
			if(paused){
				// do nothing when game is paused
				return;
			}
			time --;
			if(shaking){
				// if phone is shaking, turn light to green and add a random item
				lightColor = Color.GREEN;
				items.addRandomItem(itemsPerSec);
				gameInfo[GoldCatcher.TIME]++;
			} else {
				if(lightColor == Color.GREEN){
					// if stop shaking, turn green light to yellow as a warning
					lightColor = Color.YELLOW;
				} else {
					// if still not shaking, turn light to red and give the punishment
					gameInfo[GoldCatcher.RED_LIGHT]++;
					lightColor = Color.RED;
					items.punish();
				}
			}
		}
	};
	private TimerTask policeTask = new TimerTask(){

		@Override
		public void run() {
			// TODO execute the policeAct

			policeHandler.post(policeAct);
		}

	};

	private int changeCatcherImgCounter = 0;
	private Timer tickTimer = new Timer();
	private Handler tickHandler = new Handler();
	private Runnable tickAct = new Runnable(){

		@Override
		public void run() {
			// let the items fall
			if(time <= 0){
				tickTimer.cancel();
				tickTimer.purge();
				return;
			}
			if(paused) return;
			items.fall();
			if(changeCatcherImgCounter>=200){
				changeCatcherImgCounter = 0;
			}
			else
				changeCatcherImgCounter++;
			if(buttonLeft&&buttonRight){
				
			} else if(buttonRight){
				catcher.setX(
						Math.min(catcher.getX()+MOVE_RATE,wTree));
			} else if(buttonLeft){
				catcher.setX(
						Math.max(catcher.getX()-MOVE_RATE,0));
			}
		}

	};
	private TimerTask tickTask = new TimerTask(){

		@Override
		public void run() {

			tickHandler.post(tickAct);
		}

	};

	private abstract class Item{
		// an abstract class for item (super of gold, bomb and rock)
		private float posX = 0;
		private float posY = 0;
		private float velocity = 0;
		private boolean falling = true;
		private float radius = hTree/50;

		public Item(){
			this.posX = (float) Math.random() * wTree;
			if(posX<=wTree/2){
				posY = hTree/2 - (hTree*posX/wTree) * (float)Math.random();
			} else {
				posY = hTree/2 - (hTree - hTree*posX/wTree) * (float)Math.random();
			}
			this.velocity = (float)hTree/ (LAND_IN * TICK_RATE);
			//Log.d(TAG, "V = " + this.velocity);
		}

		public float getRadius(){
			return this.radius;
		}

		public float getX(){
			return posX;
		}

		public float getY(){
			return posY;
		}

		public void fall(){
			if(!this.falling) return;
			if(posY <= (hTree - 2*catcher.getRadius()-radius))
				this.posY += velocity;
			else if(posY >= (hTree - 2*catcher.getRadius()-radius)){
				Log.d(TAG, "posY="+posY+", catcherR="+catcher.getRadius()+", score="+this.getScore());
				if(overlap(posX,posY,catcher.getX(),catcher.getY(),radius+catcher.getRadius())){
					score += this.getScore();
					if(this.getScore()>0)
						gameInfo[GoldCatcher.GOLD]++;
					else
						gameInfo[GoldCatcher.ROCK]++;
				}
				this.falling = false;
			}
		}

		public boolean isFalling(){
			return falling;
		}

		public abstract int getColor();

		public abstract int getScore();
	}

	private class Gold extends Item{
		private int score = 10;

		public Gold() {
			super();
			//
		}

		@Override
		public int getColor() {
			// TODO Auto-generated method stub
			return Color.YELLOW;
		}

		@Override
		public int getScore() {
			// TODO Auto-generated method stub
			return this.score;
		}
	}

	private class Rock extends Item{
		private int score = -5;

		public Rock(){
			super();

		}

		@Override
		public int getColor() {
			// TODO Auto-generated method stub
			return Color.GRAY;
		}

		@Override
		public int getScore() {
			// TODO Auto-generated method stub
			return this.score;
		}
	}

	private class ItemBag {
		private ArrayList<Item> items = new ArrayList<Item>();

		public void addRandomItem(int num){
			Log.d(TAG, "addRandomItem()");
			while(num>0){
				if(Math.random()<0.5f){
					items.add(new Gold());
				} else {
					items.add(new Rock());
				}
				num--;
			}
		}

		public ArrayList<Item> getItems(){
			return items;
		}

		public void fall(){
			for(Item i : items){
				if(i.isFalling())
					i.fall();
			}
		}

		public void punish(){
			items = new ArrayList<Item>();
		}

	}

	private class Catcher{
		private float posX = 0;
		private float posY = hTree;
		private float radius = hTree/20;
		private float senseMargin = 5;
		private boolean inControl = false;

		public float getRadius(){
			return this.radius;
		}

		public float getX() {
			//get the x position of the catcher
			return this.posX;
		}

		public float getY() {
			// get the y position of the catcher
			return this.posY;
		}

		public void setX(float x){
			this.posX = x;
		}

		public void setY(float y){
			this.posY = y;
		}

		public void setRadius(float r){
			this.radius = r;
		}

		public boolean getStatus(){
			return this.inControl;
		}

		public void grabMe(float x, float y){
			this.inControl = overlap(x,y,this.posX,this.posY,
					this.radius+this.senseMargin);
		}

		public void grabMe(){
			this.inControl = true;
		}

		public void releaseMe(){
			this.inControl = false;
		}
	}

	public GCTutorial(Context context) {
		super(context); 
		goldCatcherGame = (GoldCatcherGame) context;
		// Get an instance of the SensorManager
		gcSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		//gcSensorManager = SensorManagerSimulator.getSystemService(context, Context.SENSOR_SERVICE);
		//gcSensorManager.connectSimulator();

		// Get an instance of the accelerometer sensor
		gcAccelerometer = gcSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

		// Get an instance of the WindowManager
		gcWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		gcDisplay = gcWindowManager.getDefaultDisplay();

		if(!gameInProgress){
			// kick off the anti-cheating timer thread
			policeTimer.schedule(policeTask, 0, 1000);
			// kick off the on tick falling timer thread
			tickTimer.schedule(tickTask, 0, 1);
		}

		this.gameInProgress = true;
	}

	private boolean overlap(float x1,float y1,float x2,float y2,float d){
		return (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2) <= d*d;
	}

	public void startSimulation(){
		gcSensorManager.registerListener(this, gcAccelerometer, SensorManager.SENSOR_DELAY_GAME);
	}

	public void stopSimulation(){
		gcSensorManager.unregisterListener(this);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh){
		this.wTree = w ;
		this.hTree = h ;
		//get the tree scaled to fit the screen
		Bitmap tree = BitmapFactory.decodeResource(getResources(), R.drawable.gc_tree);
		gcTree = Bitmap.createScaledBitmap(tree, w, h, true);

		// initial the position of the catcher
		catcher.setY((float)(h - w/10));
		catcher.setX(w* 0.5f);
		catcher.setRadius((float)w/20);
		this.MOVE_RATE = (float) w/1000;
		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.catcher_open);
		catcherOpen = Bitmap.createScaledBitmap(bitmap, w/10, w/10, true);
		bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.catcher_close);
		catcherClose = Bitmap.createScaledBitmap(bitmap, w/10, w/10, true);
		bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.gold);
		goldImg = Bitmap.createScaledBitmap(bitmap, h/25, h/25, true);
		bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.rock);
		rockImg = Bitmap.createScaledBitmap(bitmap, h/25, h/25, true);
		bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.gc_back_button);
		backButton = Bitmap.createScaledBitmap(bitmap, h/10, h/10, true);
		backToMainButton = Bitmap.createScaledBitmap(bitmap, w/3, w/3, true);
		bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.gc_pause_button);
		pauseButton = Bitmap.createScaledBitmap(bitmap, h/10, h/10, true);
		bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.gc_resume_button);
		resumeButton = Bitmap.createScaledBitmap(bitmap, w/3, w/3, true);

	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER){
			return;
		}

		switch (gcDisplay.getRotation()) {
		case Surface.ROTATION_0:
			gcSensorX = event.values[0];
			gcSensorY = event.values[1];
			break;
		case Surface.ROTATION_90:
			gcSensorX = -event.values[1];
			gcSensorY = event.values[0];
			break;
		case Surface.ROTATION_180:
			gcSensorX = -event.values[0];
			gcSensorY = -event.values[1];
			break;
		case Surface.ROTATION_270:
			gcSensorX = event.values[1];
			gcSensorY = -event.values[0];
			break;
		}
		gravity[1] = 0.9f * gravity[1] + 0.1f * gcSensorY;
		gcSensorY -= gravity[1];
		if(gcSensorY > 2 || gcSensorY < -2){
			//shaking = true;
			//items.fall();
		} else {
			//shaking = false;
		}
		if(GCShared.control == GCShared.TILT){
			if(gcSensorX>1){
				this.buttonLeft = false;
				this.buttonRight = true;
			} else if(gcSensorY<-1){
				this.buttonRight = false;
				this.buttonLeft = true;
			} else {
				this.buttonLeft = false;
				this.buttonRight = false;
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event){
		if(event.getAction()==MotionEvent.ACTION_DOWN){
			if(inCircle(event.getX(),event.getY(),
					hTree*0.05f, hTree*0.05f, hTree*0.05f)){
				backToMain();
				return true;
			}else if(inCircle(event.getX(),event.getY(),
					wTree-hTree*0.05f, hTree*0.05f, wTree*0.05f)){
				startGame();
				return true;
			}
		}
		
		if(event.getAction()==MotionEvent.ACTION_DOWN){
			if(GCShared.control == GCShared.SLIDE){
				//touch and slide control
				catcher.grabMe(event.getX(),event.getY());
				return catcher.getStatus();
			} else if(GCShared.control == GCShared.BUTTON){
				//button press control
				if(event.getX()<wTree*0.25f&&event.getY()>catcher.getY()){
					Log.d(TAG, "Left button pressed");
					this.buttonLeft = true;
				} else if(event.getX()>wTree*0.75f&&event.getY()>catcher.getY()){
					Log.d(TAG, "Right button pressed");
					this.buttonRight = true;
				}
			} else {
				return super.onTouchEvent(event);
			}
			return true;
		}
		if(event.getAction()==MotionEvent.ACTION_MOVE){
			if(GCShared.control == GCShared.SLIDE
					&& catcher.getStatus()){
			catcher.setX(event.getX());
			return true;
			}
		}
		if(event.getAction()==MotionEvent.ACTION_UP){
			Log.d(TAG, "Button Released!");
			if(GCShared.control == GCShared.SLIDE
					&& catcher.getStatus()){
			catcher.releaseMe();
			return true;
			} else if(GCShared.control == GCShared.BUTTON){
				Log.d(TAG, "Button Released!");
				this.buttonLeft = false;
				this.buttonRight = false;
				return true;
			}
		}

		return super.onTouchEvent(event);
	}

	private void backToMain() {
		Intent backToMain = new Intent(this.goldCatcherGame,GoldCatcher.class);
		this.goldCatcherGame.startActivity(backToMain);
	}
	
	private void startGame(){
		Intent startGame = new Intent(this.goldCatcherGame, GoldCatcherGame.class);
		this.goldCatcherGame.startActivity(startGame);
	}

	private boolean inCircle(float x, float y, float cX, float cY, float r) {
		return (x-cX)*(x-cX) + (y-cY)*(y-cY) <= r*r;
	}

	@Override
	protected void onDraw(Canvas canvas){
		// Draw the tree
		canvas.drawBitmap(gcTree, 0, 0, null);

		if(this.paused){
			canvas.drawBitmap(resumeButton, wTree*0.25f-wTree*0.1667f, 
					hTree/2-wTree*0.1667f, null);
			canvas.drawBitmap(backToMainButton, wTree*0.75f-wTree*0.1667f,
					hTree/2-wTree*0.1667f, null);
			return;
		} else {
			//buttons
			canvas.drawBitmap(backButton, 0, 0, null);
			canvas.drawBitmap(pauseButton, wTree - hTree*0.1f, 0, null);
		}


		// Draw the items
		for(Item i : items.getItems()){
			if(i.isFalling()){
				if(i.getColor()==Color.YELLOW)
					canvas.drawBitmap(goldImg, i.getX()-i.getRadius(), i.getY()-i.getRadius(), null);
				else
					canvas.drawBitmap(rockImg, i.getX()-i.getRadius(), i.getY()-i.getRadius(), null);
			}
		}

		// Draw the warning light
		Paint lightPaint = new Paint();
		lightPaint.setStyle(Style.FILL);
		lightPaint.setColor(lightColor);
		canvas.drawRect((float)wTree/3, 0, (float)wTree*2/3, (float)hTree/10, lightPaint);

		// Score and time
		Paint timePaint = new Paint();
		timePaint.setTextAlign(Align.CENTER);
		timePaint.setTextSize((float)wTree/20);
		canvas.drawText("Tutorial",(float)wTree/2, (float)hTree/10, timePaint);

		// Draw the catcher
		if(this.changeCatcherImgCounter < 100)
			canvas.drawBitmap(catcherOpen, catcher.getX()-catcher.getRadius(), 
					catcher.getY()-catcher.getRadius(), null);
		else
			canvas.drawBitmap(catcherClose, catcher.getX()-catcher.getRadius(), 
					catcher.getY()-catcher.getRadius(), null);

		if(GCShared.control == GCShared.BUTTON){
			// Draw the control button
			Path arrow = new Path();
			arrow.moveTo(0, catcher.getY()+catcher.getRadius());
			arrow.lineTo(wTree/4, catcher.getY());
			arrow.lineTo(wTree/4, catcher.getY()+catcher.getRadius()*2);
			arrow.lineTo(0, catcher.getY()+catcher.getRadius());
			arrow.moveTo(wTree, catcher.getY()+catcher.getRadius());
			arrow.lineTo(wTree*3/4, catcher.getY());
			arrow.lineTo(wTree*3/4, catcher.getY()+catcher.getRadius()*2);
			arrow.lineTo(wTree, catcher.getY()+catcher.getRadius());
			Paint arrowPaint = new Paint();
			arrowPaint.setStyle(Style.FILL);
			arrowPaint.setColor(Color.LTGRAY);
			if(this.buttonLeft||this.buttonRight)
				arrowPaint.setAlpha(300);
			else
				arrowPaint.setAlpha(150);
			canvas.drawPath(arrow, arrowPaint);
		}

		invalidate();
	}

}