package edu.neu.madcourse.yuntaozhang.goldcatcher;

import edu.neu.madcourse.yuntaozhang.R;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.app.Activity;
import android.view.Menu;

public class GoldCatcherGame extends Activity {
	private final static String TAG = "GoldCathcerGame";

	private GoldCatcherGameView gcGameView;
	
	private PowerManager gcPowerManager;
	
	private WakeLock gcWakeLock;
	
	public static final String TIME = "Current time left";
	public static final String SCORE = "Current Score";
	public static final String WORKOUT = "Workout time so far";
	public static final String RED_LIGHT = "Red light last time";
	public static final String GOLD = "Total gold caught";
	public static final String ROCK = "Total rocks caught";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Get an instance of the PowerManager
		gcPowerManager = (PowerManager) getSystemService(POWER_SERVICE);

		// Create a bright wake lock
		gcWakeLock = gcPowerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, getClass()
				.getName());

		// get a instance of the game view
		gcGameView = new GoldCatcherGameView(this);
		
		// turn the boolean to indicate that the game has started
		GCShared.onGame = true;
		
		//gcGameView.setBackgroundResource(R.drawable.gc_background);
		setContentView(gcGameView);
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		
		// acquire wake lock
		gcWakeLock.acquire();
		
		gcGameView.onResume();
		
		//start sensing
		gcGameView.startSimulation();
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		
		// release wake lock
		gcWakeLock.release();
		
		gcGameView.onPause();
		
		//stop sensing
		gcGameView.stopSimulation();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gold_catcher_game, menu);
		return true;
	}

}
