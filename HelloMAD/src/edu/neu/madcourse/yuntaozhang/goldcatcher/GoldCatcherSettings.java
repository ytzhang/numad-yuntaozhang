package edu.neu.madcourse.yuntaozhang.goldcatcher;

import edu.neu.madcourse.yuntaozhang.R;
import edu.neu.madcourse.yuntaozhang.R.layout;
import edu.neu.madcourse.yuntaozhang.R.menu;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioGroup;
import android.widget.ToggleButton;

public class GoldCatcherSettings extends Activity implements OnClickListener{
	private int control;
	private boolean tutorial;
	private boolean music;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gold_catcher_settings);
		this.control = GCShared.control;
		this.tutorial = GCShared.tutorial;
		this.music = GCShared.music;
		RadioGroup options = (RadioGroup) findViewById(R.id.gc_settings_options);
		switch(control){
		case GCShared.SLIDE:
			options.check(R.id.gc_settings_touch);
			break;
		case GCShared.BUTTON:
			options.check(R.id.gc_settings_press);
			break;
		case GCShared.TILT:
			options.check(R.id.gc_settings_tilt);
			break;
		default:
			options.check(R.id.gc_settings_touch);
		}
		ToggleButton tutorialButton = (ToggleButton) findViewById(R.id.gc_settings_tutorial);
		tutorialButton.setChecked(tutorial);
		ToggleButton musicButton = (ToggleButton) findViewById(R.id.gc_settings_music);
		musicButton.setChecked(music);
		findViewById(R.id.gc_settings_save).setOnClickListener(this);
		findViewById(R.id.gc_settings_cancel).setOnClickListener(this);
		findViewById(R.id.gc_settings_press).setOnClickListener(this);
		findViewById(R.id.gc_settings_tilt).setOnClickListener(this);
		findViewById(R.id.gc_settings_touch).setOnClickListener(this);
		findViewById(R.id.gc_settings_tutorial).setOnClickListener(this);
		findViewById(R.id.gc_settings_music).setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gold_catcher_settings, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.gc_settings_touch:
			this.control = GCShared.SLIDE;
			break;
		case R.id.gc_settings_press:
			this.control = GCShared.BUTTON;
			break;
		case R.id.gc_settings_tilt:
			this.control = GCShared.TILT;
			break;
		case R.id.gc_settings_tutorial:
			this.tutorial = !this.tutorial;
			break;
		case R.id.gc_settings_music:
			this.music = !this.music;
			break;
		case R.id.gc_settings_save:
			saveChanges();
			finish();
			break;
		case R.id.gc_settings_cancel:
			finish();
			break;
		}

	}

	private void saveChanges() {
		// save the changes
		GCShared.control = this.control;
		GCShared.tutorial = this.tutorial;
		GCShared.music = this.music;
	}

}
