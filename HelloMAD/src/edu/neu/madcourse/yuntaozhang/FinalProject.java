package edu.neu.madcourse.yuntaozhang;

import edu.neu.madcourse.yuntaozhang.goldcatcher.GoldCatcher;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class FinalProject extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_final_project);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_final_project, menu);
		return true;
	}
	
	public void startGoldCatcher(View v){
		Intent goldCatcher = new Intent(this, GoldCatcher.class);
		startActivity(goldCatcher);
	}

}
