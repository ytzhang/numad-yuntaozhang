package edu.neu.madcourse.yuntaozhang.boggle;

import java.util.ArrayList;

public class BoggleSharedData {
	private static int boggleGameStatus = BogglePuzzleView.STOPPED;
	private static ArrayList<String> usedWords = new ArrayList<String>();
	private static boolean boggleMusicPref = true;
	
	public static void setGameStatus(int status){
		boggleGameStatus = status;
	}
	public static int getGameStatus(){
		return boggleGameStatus;
	}
	public static ArrayList<String> getUsedWords() {
		return usedWords;
	}
	public static void setUsedWords(ArrayList<String> usedWords) {
		BoggleSharedData.usedWords = usedWords;
	}
	public static boolean getBoggleMusicPref() {
		return boggleMusicPref;
	}
	public static void setBoggleMusicPref(boolean musicPref) {
		boggleMusicPref = musicPref;
	}

}
