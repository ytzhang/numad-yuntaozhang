package edu.neu.madcourse.yuntaozhang.boggle;

import edu.neu.madcourse.yuntaozhang.HelloMAD;
import edu.neu.madcourse.yuntaozhang.Music;
import edu.neu.madcourse.yuntaozhang.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

public class Boggle extends Activity implements OnClickListener{
	private static final String TAG = "Boggle";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.boggle_main);
		int onGame = BoggleSharedData.getGameStatus();
		View continueButton = findViewById(R.id.continue_boggle_button);
		if(onGame!=BogglePuzzleView.RESUMED
				&&onGame!=BogglePuzzleView.PAUSED){
			continueButton.setVisibility(View.GONE);
			}
		else{
			continueButton.setOnClickListener(this);
		}
		View acknowledgementsButton = findViewById(R.id.acknowledgements_boggle_button);
		acknowledgementsButton.setOnClickListener(this);
		
		View newGameButton = findViewById(R.id.new_boggle_button);
		newGameButton.setOnClickListener(this);
		
		View exitButton = findViewById(R.id.exit_boggle_button);
		exitButton.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_boggle, menu);
		return true;
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		Music.playBoggle(this, R.raw.boggle);
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		Music.stop(this);
	}

	public void onClick(View v) {
		switch(v.getId()){
		case R.id.acknowledgements_boggle_button:
			Intent acknowledgements = new Intent(this, BoggleAcknowledgements.class);
			startActivity(acknowledgements);
			break;
		case R.id.continue_boggle_button:
			Intent continueGame = new Intent(this, BoggleGame.class);
			continueGame.putExtra(BoggleGame.KEY_DIFFICULTY, BoggleGame.DIFFICULTY_CONTINUE);
		      startActivity(continueGame);
		      break;
		case R.id.new_boggle_button:
			Intent boggleGame = new Intent(this, BoggleGame.class);
			startActivity(boggleGame);
			break;
		case R.id.exit_boggle_button:
			Intent backToMain = new Intent(this, HelloMAD.class);
			startActivity(backToMain);
			break;
		}		
	}
	
}
