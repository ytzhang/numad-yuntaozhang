package edu.neu.madcourse.yuntaozhang.boggle;

import java.util.Random;

import edu.neu.madcourse.yuntaozhang.Music;
import edu.neu.madcourse.yuntaozhang.R;
import edu.neu.madcourse.yuntaozhang.R.layout;
import edu.neu.madcourse.yuntaozhang.R.menu;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;

public class BoggleGame extends Activity {
	private static final String TAG = "Boggle";

	private BogglePuzzleView bogglePuzzleView;
	private char puzzle[] = new char[4*4];

	public static final int DIFFICULTY_EASY = 0;
	public static final int DIFFICULTY_CONTINUE = -1;

	public static final String KEY_DIFFICULTY = 
			"edu.neu.madcourse.yuntaozhang.difficulty";

	private final String PREF_PUZZLE  = "prefPuzzle";

	private int gameDiff = DIFFICULTY_EASY;

	private final String easyPuzzle = 
			"AAAAABBCCCDDDEEEEFFGGGHHHIIIJKKLLL"
					+"MMMNNNOOOPPPQRRRSSSTTTUUUVVWWXXYYZZ";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		gameDiff = getIntent().getIntExtra(KEY_DIFFICULTY,
				DIFFICULTY_EASY);
		puzzle = getPuzzle(gameDiff);
		bogglePuzzleView = new BogglePuzzleView(this);
		setContentView(bogglePuzzleView);
		bogglePuzzleView.requestFocus();

	}
	@Override
	protected void onResume() {
		super.onResume();
		Music.playBoggle(this, R.raw.bogglegame);
		if(BoggleSharedData.getGameStatus()==BogglePuzzleView.STOPPED)
			gameDiff = DIFFICULTY_EASY;
		if(gameDiff==DIFFICULTY_CONTINUE)
			bogglePuzzleView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause");
		Music.stop(this);
		// Save the current puzzle
		bogglePuzzleView.onPause();
		getPreferences(MODE_PRIVATE).edit().putString("MYPUZZLE", toPuzzleString()).commit();	      
	}


	private String toPuzzleString() {
		String currentPuzzle = "";
		for(char letter : puzzle)
			currentPuzzle+=letter;
		return currentPuzzle;
	}
	/** Given a difficulty level, come up with a new puzzle */
	private char[] getPuzzle(int diff) {
		String puz;
		switch (diff) {
		case DIFFICULTY_CONTINUE:
			puz = getPreferences(MODE_PRIVATE).getString("MYPUZZLE",
					easyPuzzle);
			break;
		case DIFFICULTY_EASY:
			puz = easyPuzzle;
			break;
		default:
			puz = easyPuzzle;
			break;
		}
		return fromPuzzleString(puz,diff);
	}

	private char[] fromPuzzleString(String puz, int diff) {
		int i =0;
		char[] newPuz = new char[4*4];
		if(diff==DIFFICULTY_CONTINUE){
			for(i=0; i<4*4; i++)
				newPuz[i]=puz.charAt(i);
		} else {
			Random random = new Random();
			for(i = 0; i<4*4; i++){
				newPuz[i] = puz.charAt(random.nextInt(puz.length()));
			}
		}
		return newPuz;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_boggle_game, menu);
		return true;
	}

	/** Return the tile at the given coordinates */
	private char getTile(int x, int y) {
		return puzzle[y * 4 + x];
	}

	/** Change the tile at the given coordinates */
	private void setTile(int x, int y, char value) {
		puzzle[y * 4 + x] = value;
	}

	/** Return a string for the tile at the given coordinates */
	protected String getTileString(int x, int y) {
		char v = getTile(x, y);
		if (v == 0)
			return "";
		else
			return String.valueOf(v);
	}

	/** Return the current puzzle char array*/
	protected char[] getPuzzleChars(){
		return this.puzzle;
	}

	/** Set the current puzzle car array with the give chars*/
	protected void setPuzzleChars(char[] newPuzzle){
		this.puzzle=newPuzzle;
	}

}
