package edu.neu.madcourse.yuntaozhang.boggle.persistent;

import edu.neu.madcourse.yuntaozhang.R;
import edu.neu.madcourse.yuntaozhang.R.layout;
import edu.neu.madcourse.yuntaozhang.R.menu;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class BoggleAcknowledgements extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.boggle_persistent_acknowledgements);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.boggle_persistent_acknowledgements,
				menu);
		return true;
	}

}
