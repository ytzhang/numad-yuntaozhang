package edu.neu.madcourse.yuntaozhang.boggle.persistent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import edu.neu.madcourse.yuntaozhang.Music;
import edu.neu.madcourse.yuntaozhang.R;
import edu.neu.mobileclass.apis.KeyValueAPI;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.format.Time;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

public class BogglePuzzleView extends View {

	private static final String TAG = "Boggle";
	private final BoggleGame boggleGame;
	private int ID = 43;



	public int gameStatus=RESUMED;
	public static int STOPPED = 0;
	public static int RESUMED = 1;
	public static int PAUSED = 2;
	private boolean isPaused = false;
	private String pauseMsg = "Paused";
	private String timeUpMsg = "Time's Up!";

	private float width;    // width of one tile
	private float height;   // height of one tile
	private int selX;       // X index of selection
	private int selY;       // Y index of selection
	private final Rect selRect = new Rect();

	private ArrayList<Integer> selectedTiles= new ArrayList<Integer>();

	private final Path arrow = new Path();
	private final Point arrowP1 = new Point();
	private final Point arrowP2 = new Point();
	private final Point arrowP3 = new Point();	
	private int score = 0;
	private int opponentScore = 0;
	private boolean oppDone = false;
	private boolean hostDone = false;
	private int time = 60;
	private int personalBest = 0;
	private static final String HIGH_SCORE = "PersonalBest";

	protected void setTime(int newTime){
		time=newTime;
	}

	protected void setScore(int newScore){
		score = newScore;
	}

	Timer timer = new Timer();

	public static String TIME = "time";
	public static String SCORE = "score";
	public static String STATUS = "status";
	public static String GAME_BOARD = "gameBoard";
	public static String USED_WORDS = "usedWords";

	private static final String VIEW_STATE = "viewState";

	private final Handler countDownHandler = new Handler();
	private final Runnable startCountDown = new Runnable(){
		public void run(){
			if(time<=0&&gameStatus==RESUMED) {
				timer.cancel();
				gameStatus = STOPPED;
				BoggleSharedData.setGameStatus(STOPPED);
				hostDone = true;
				updateBestScore();
				Music.stop(boggleGame);
				invalidate();
			}
			else invalidate();
		}

		private void updateBestScore() {
			//
			if(personalBest < score){
				personalBest = score;
				SharedPreferences.Editor editor = boggleGame.getPreferences(Context.MODE_PRIVATE)
						.edit();
				editor.putInt(HIGH_SCORE, score);
				editor.commit();
			}
		}
	};
	TimerTask countDown = new TimerTask(){
		@Override
		public void run(){
			countDown();
		}
	};

	private String input = "";
	private boolean dragging = false;
	private ArrayList<String> usedWords = new ArrayList<String>();
	private int numWords = 0;
	String tempWords = "";


	public BogglePuzzleView(Context context) {
		super(context);
		this.boggleGame = (BoggleGame) context;
		setFocusable(true);
		setFocusableInTouchMode(true);
		BoggleSharedData.setGameStatus(gameStatus);
		//kick off the timer
		if(gameStatus!=RESUMED||gameStatus!=PAUSED)
			timer.schedule(countDown, 0, 1000);

		//spawn a thread to check server opponent words update
		new syncServerTask().execute();

		SharedPreferences topScore = boggleGame.
				getPreferences(Context.MODE_PRIVATE);
		personalBest = topScore.getInt(HIGH_SCORE, 0);

		setId(ID); 
	}



	private class syncServerTask extends AsyncTask<String, String, String>{
		private final static String teamName = BoggleSharedData.teamName;
		private final static String password = BoggleSharedData.password;
		private final static String error = BoggleSharedData.error;
		private static final String CLEAR_TEMP = "Saved and clear temp";
		private static final String UPDATE_TEMP = "Fetch temp to used words";
		private static final String OPP_DONE = "Opponent Done!";
		private static final String SCORE = "score";
		private String words = "";
		private String hostPhone = BoggleSharedData.getHostPhone();
		private String opponentPhone = BoggleSharedData.getOpponentPhone();
		private String usedWordsHere = "";
		private int hostScore = 0;
		private String oppScore = "0";

		@Override
		protected String doInBackground(String... params) {
			//clearKeys();
			KeyValueAPI.clearKey(teamName, password, BoggleSharedData.status+hostPhone);
			KeyValueAPI.clearKey(teamName, password, BoggleSharedData.words+hostPhone);
			KeyValueAPI.clearKey(teamName, password, FindMatch.PUZ+hostPhone);
			// check the update in opponent used words updates
			while(!hostDone){
				//get words from opponent
				while((words = KeyValueAPI.get(teamName, password, 
						BoggleSharedData.words+opponentPhone))
						.contains(error));

				//add opponent words to used words list
				if(words.length()>0){
					for(String word : words.split(",")){
						publishProgress(word);
					}
				}
				publishProgress(UPDATE_TEMP);
				//add host's used words list
				if(usedWordsHere.length()>0){
					while(KeyValueAPI.put(teamName, password, 
							BoggleSharedData.words + hostPhone, usedWordsHere).contains(error));
					usedWordsHere = "";
					publishProgress(CLEAR_TEMP);
				}
			}

			//clear all keys created by this user for this game when stopped
			if(hostDone){
				while(KeyValueAPI.put(teamName, password, SCORE+hostPhone, ""+hostScore)
						.contains(error));
				while(KeyValueAPI.put(teamName, password, 
						BoggleSharedData.status + hostPhone, OPP_DONE).contains(error));
				while(!KeyValueAPI.get(teamName, password, 
						BoggleSharedData.status + opponentPhone).equals(OPP_DONE));
				while((oppScore = KeyValueAPI.get(teamName, password, SCORE+opponentPhone))
						.contains(error));
				publishProgress(UPDATE_TEMP);
				publishProgress(OPP_DONE);
			}
			clearKeys();
			updateTopList();
			return null;  // return value is ignored
		}

		private void updateTopList() {
			// update the top score list if necessary
			String top5 = "";
			while((top5=KeyValueAPI.get(teamName, password, BoggleSharedData.TOP+5))
					.contains(error));
			if(top5.length()<=0 || Integer.valueOf(top5)<hostScore){
				Time today = new Time(Time.getCurrentTimezone());
				today.setToNow();
				String time = today.format("%k:%M:%S") + " " +
						today.monthDay + "." + today.month+ "." + today.year;
				addToTop(hostPhone, hostScore, time);
			}
			else return;
		}

		private void addToTop(String newPhone, int newScore, String newTime) {
			// add the host score and achieved time to top list

			int rank = 5;
			int top = 4;
			String scoreTop = "";
			for(top=4; top>0; top--){
				while((scoreTop=KeyValueAPI.get(teamName, password, BoggleSharedData.TOP+top))
						.contains(error));
				if(scoreTop.length()<=0 || Integer.valueOf(scoreTop)<newScore)
					rank--;
				else 
					break;
			}

			addToList(rank, newPhone, newScore, newTime);
		}

		private void addToList(int rank, String newPhone, int newScore,
				String newTime) {
			// add the info to top list at rank
			if(rank<=5){
				String nextPhone = "";
				String nextScore = "";
				String nextTime="";
				// Get current info at rank
				while((nextPhone = KeyValueAPI.get(teamName, password, BoggleSharedData.ID_TOP+rank))
						.contains(error));
				while((nextScore=KeyValueAPI.get(teamName, password, BoggleSharedData.TOP+rank))
						.contains(error));
				while((nextTime=KeyValueAPI.get(teamName, password, BoggleSharedData.TIME_TOP+rank))
						.contains(error));
				//Store the score and time at rank	
				while(KeyValueAPI.put(teamName, password, BoggleSharedData.ID_TOP+rank, ""+newPhone)
						.contains(error));
				while(KeyValueAPI.put(teamName, password, BoggleSharedData.TOP+rank, ""+newScore)
						.contains(error));
				while(KeyValueAPI.put(teamName, password, BoggleSharedData.TIME_TOP+rank, newTime)
						.contains(error));
				if(nextPhone.length()<=0 || nextScore.length()<=0 || nextTime.length()<=0)
					return;
				else 
					addToList(rank+1, nextPhone, Integer.valueOf(nextScore),nextTime);
			} else {
				return;
			}

		}

		@Override
		protected void onPostExecute(String result){
			opponentScore = Integer.parseInt(oppScore);

		}

		private void clearKeys() {
			// clear key created by this match
			String result = "ERROR";
			while(result.equalsIgnoreCase(error))
				result = KeyValueAPI.clearKey(teamName, password, hostPhone)
				+ KeyValueAPI.clearKey(teamName, password, BoggleSharedData.words+hostPhone)
				+ KeyValueAPI.clearKey(teamName, password, FindMatch.PUZ+hostPhone);
		}

		@Override
		protected void onProgressUpdate(String... progress){
			if(progress[0].equalsIgnoreCase(CLEAR_TEMP)){
				tempWords="";
				hostScore = score;
			}
			else if(progress[0].equalsIgnoreCase(UPDATE_TEMP)) {
				usedWordsHere = tempWords;
				if(oppScore.length()>0)
					opponentScore = Integer.valueOf(oppScore);
				else
					opponentScore = 0;
			}
			else if(progress[0].equalsIgnoreCase(OPP_DONE)){
				oppDone = true;
				invalidate();
			}
			else{
				if(!usedWords.contains(progress[0])){
					addToUsedWords(progress[0]);
					if(boggleGame.inGame)
						Toast.makeText(getContext(), progress[0]+" is found!", 
								Toast.LENGTH_SHORT).show();
					else 
						boggleGame.pushNotice(progress[0]);
				}
			}
		}

		private void sleep(int sec){
			try {
				Thread.sleep(sec*1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected Parcelable onSaveInstanceState() { 
		Parcelable p = super.onSaveInstanceState();
		Log.d(TAG, "onSaveInstanceState");
		Bundle bundle = new Bundle();
		bundle.putInt(TIME, time);
		bundle.putInt(SCORE, score);
		bundle.putInt(STATUS, gameStatus);
		bundle.putCharArray(GAME_BOARD, boggleGame.getPuzzleChars());
		bundle.putStringArrayList(USED_WORDS, usedWords);
		bundle.putParcelable(VIEW_STATE, p);
		return bundle;
	}
	
	@Override
	protected void onRestoreInstanceState(Parcelable state) { 
		Log.d(TAG, "onRestoreInstanceState");
		Bundle bundle = (Bundle) state;
		time = bundle.getInt(TIME);
		score = bundle.getInt(SCORE);
		boggleGame.setPuzzleChars(bundle.getCharArray(GAME_BOARD));
		gameStatus= bundle.getInt(STATUS);
		usedWords = bundle.getStringArrayList(USED_WORDS);
		super.onRestoreInstanceState(bundle.getParcelable(VIEW_STATE));
	}
	public void onPause(){
		Log.d(TAG,"onPause()");
		boggleGame.getPreferences(Context.MODE_PRIVATE).edit().putInt(TIME, time).commit();
		boggleGame.getPreferences(Context.MODE_PRIVATE).edit().putInt(SCORE, score).commit();
		BoggleSharedData.setUsedWords(usedWords);
	}
	public void onResume(){
		Log.d(TAG,"onResume()");
		time = boggleGame.getPreferences(Context.MODE_PRIVATE).getInt(TIME, time);
		score = boggleGame.getPreferences(Context.MODE_PRIVATE).getInt(SCORE, score);
		usedWords=BoggleSharedData.getUsedWords();
		gameStatus = BoggleSharedData.getGameStatus();
		if(time<=0||gameStatus==STOPPED) {
			Intent newGame = new Intent(boggleGame, BoggleGame.class);
			boggleGame.startActivity(newGame);
		}else{
			if(gameStatus==PAUSED) isPaused = true;
			else isPaused = false;
		}
	}
	private void countDown() {
		if(isPaused) return;
		time--;
		countDownHandler.post(startCountDown);
	}



	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh){
		width = w / 5f;
		height = h / 7f;
		getRect(selX, selY, selRect);
	}

	private void getRect(int x, int y, Rect rect) {
		rect.set((int)(x*width),(int)(1*height + y*height),
				(int)(x*width+width),(int)(y*height+2*height));	
	}

	@Override
	protected void onDraw(Canvas canvas){
		//Draw the background
		Paint backgraound = new Paint();
		backgraound.setColor(getResources().
				getColor(R.color.puzzle_background));
		canvas.drawRect(0, 0, getWidth(), getHeight(), backgraound);

		//Draw the game board
		//Define colors for the grid lines
		Paint hilite = new Paint();
		hilite.setColor(getResources().getColor(R.color.puzzle_hilite));
		Paint light = new Paint();
		light.setColor(getResources().getColor(R.color.puzzle_light));

		//Draw the minor grid lines
		for(int i=0; i <= 5 ; i ++){
			canvas.drawLine(0, i*height+1*height+1, width*5, i*height+1*height+1, hilite);
			canvas.drawLine(i*width+1, 0+1*height, i*width+1, height*5+1*height, hilite);
			canvas.drawLine(0, i*height+1*height, width*5, i*height+1*height, light);
			canvas.drawLine(i*width, 0+1*height, i*width, height*5+1*height, light);
		}

		if(isPaused){
			//Resume Button
			Paint buttonCircle = new Paint();
			buttonCircle.setColor(getResources().getColor(R.color.boggle_resume_circle));
			float cX = getWidth()/4;
			float cY = getHeight()/2;
			float r = Math.min(width, height);
			canvas.drawCircle(cX, cY, r, buttonCircle);
			Paint mark = new Paint();
			mark.setColor(Color.LTGRAY);
			mark.setStyle(Paint.Style.STROKE);
			mark.setStrokeWidth(5);
			Path markPath = new Path();
			float[] resumeLines = {cX-r/3,cY-r/3,cX+r/3,cY,cX-r/3,cY+r/3,cX-r/3,cY-r/3 };
			markPath.moveTo(resumeLines[0], resumeLines[1]);
			markPath.lineTo(resumeLines[2], resumeLines[3]);
			markPath.lineTo(resumeLines[4], resumeLines[5]);
			markPath.lineTo(resumeLines[0], resumeLines[1]);
			//canvas.drawPath(markPath, mark);
			//Back Button
			cX = getWidth()*3/4;
			canvas.drawCircle(cX, cY, r, buttonCircle);
			markPath.moveTo(cX-2*r/3, cY);
			markPath.lineTo(cX-r/3, cY-r/3);
			markPath.lineTo(cX-r/3, cY-r/6);
			markPath.lineTo(cX+2*r/3, cY-r/6);
			markPath.lineTo(cX+2*r/3, cY+r/6);
			markPath.lineTo(cX-r/3, cY+r/6);
			markPath.lineTo(cX-r/3, cY+r/3);
			markPath.lineTo(cX-2*r/3, cY);
			markPath.close();
			canvas.drawPath(markPath, mark);
			//Pause Message
			Paint pausePaint = new Paint(Paint.ANTI_ALIAS_FLAG); 
			pausePaint.setColor(Color.BLACK);
			pausePaint.setStyle(Style.FILL);
			pausePaint.setTextSize(height * 0.75f);
			canvas.drawText(pauseMsg, width, height, pausePaint);
			pausePaint.setColor(Color.LTGRAY);
			canvas.drawText(pauseMsg, width+1, height+1, pausePaint);
		} else {
			//Pause/Submit word/cancel word Buttons
			if(gameStatus!=STOPPED){
				Paint buttons = new Paint();
				buttons.setColor(getResources().getColor(R.color.boggle_resume_circle));
				//Pause Button
				canvas.drawRect(5, 5, width-5, height-5, buttons);
				if(input.length()>2&&!dragging){
					//submit button
					canvas.drawRect(4*width+5, 6*height+5, 5*width-5, 7*height-5, buttons);
				}
				if(input.length()>0&&!dragging){
					//cancel button
					canvas.drawRect(5, 6*height+5, width-5, 7*height-5, buttons);
				}

				buttons.setColor(Color.LTGRAY);
				//pause mark
				canvas.drawRect(width/6+width/8, height/3, width/3+width/8, height*2/3, buttons);
				canvas.drawRect(width*4/6-width/8, height/3, width*5/6-width/8, height*2/3, buttons);

				//current input word
				Paint wordPaint = new Paint(Paint.ANTI_ALIAS_FLAG); 
				wordPaint.setStyle(Style.FILL);
				wordPaint.setTextSize(Math.min(height * 0.75f,3*width/(input.length()+1)));
				wordPaint.setColor(Color.BLACK);
				wordPaint.setTextAlign(Paint.Align.CENTER);
				canvas.drawText(input, 5*width/2, (int)(height*6.7), wordPaint);


				buttons.setStyle(Style.STROKE);
				buttons.setStrokeWidth(5);
				if(input.length()>2&&!dragging){
					//submit mark
					buttons.setColor(Color.RED);
					buttons.setAlpha(100);
					canvas.drawLine(4*width+width/3, 6*height+height/2, 4*width+width/2, 6*height+height*3/4, buttons);
					canvas.drawLine(4*width+width/2, 6*height+height*3/4, 4*width+width*3/4, 6*height+height/4, buttons);
				}
				if(input.length()>0&&!dragging){
					//cancel mark
					buttons.setColor(Color.BLUE);
					buttons.setAlpha(100);
					canvas.drawLine(width/4, 6*height+height/4, width*3/4, 6*height+height*3/4, buttons);
					canvas.drawLine(width/4, 6*height+height*3/4, width*3/4, 6*height+height/4, buttons);
				}
			}

			//Back Button
			Paint backButton = new Paint();
			backButton.setColor(getResources().getColor(R.color.boggle_resume_circle));
			Paint backArrow = new Paint();
			backArrow.setColor(Color.LTGRAY);
			backArrow.setStyle(Style.STROKE);
			backArrow.setStrokeWidth(5);
			float backX = 4*width;
			float backY = 0;
			float backWidth = width;
			if(gameStatus==STOPPED||time<=0) {
				//Relocate the position of back button when game over
				backX=3*width/2;
				backY=6*height;
				backWidth = 2*width;
			}
			canvas.drawRect(backX+5, backY+5, backX+backWidth-5, backY+height-5, backButton);
			canvas.drawLine(backX+backWidth/4-2, backY+height/2, backX+backWidth*3/4, backY+height/2, backArrow);
			canvas.drawLine(backX+backWidth/4, backY+height/2, backX+backWidth*3/8, backY+height/3, backArrow);
			canvas.drawLine(backX+backWidth/4, backY+height/2, backX+backWidth*3/8, backY+height*2/3, backArrow);


			// Define color and style for numbers
			Paint foreground = new Paint(Paint.ANTI_ALIAS_FLAG); 
			foreground.setColor(getResources().getColor(R.color.puzzle_foreground));
			foreground.setStyle(Style.FILL);
			foreground.setTextSize(Math.min(height,width) * 0.75f);
			//foreground.setTextScaleX(width / height);
			foreground.setTextAlign(Paint.Align.CENTER);
			// Draw the number in the center of the tile
			FontMetrics fm = foreground.getFontMetrics();
			// Centering in X: use alignment (and X at midpoint)
			float x = width / 2;
			// Centering in Y: measure ascent/descent first 
			float y = height / 2 - (fm.ascent + fm.descent) / 2;

			//Draw letters
			for (int i = 0; i < 5; i++) {
				for (int j = 0; j < 5; j++) {
					canvas.drawText(this.boggleGame.getTileString(i, j), 
							i * width + x, j * height + 1*height + y, foreground);
				}
			}	
			//Draw the Timer
			if(gameStatus!=STOPPED){
				char sec1 = time%60>9?Integer.toString(time%60).charAt(0):'0';
				char sec2 = sec1=='0'?Integer.toString(time%60).charAt(0):Integer.toString(time%60%10).charAt(0);
				foreground.setTextSize(height*0.35f);
				canvas.drawText("0"+Integer.toString(time/60)+":"+sec1+sec2, 2*width+x, y/2, foreground);
			}

			//Draw the Score board
			String scoreTxt = Integer.toString(score);
			foreground.setTextSize(height*0.38f);
			if(gameStatus==STOPPED) {
				foreground.setTextSize(height*0.75f);
				scoreTxt = "Score: "+scoreTxt;
			}
			canvas.drawText(scoreTxt, 2*width+x, y, foreground);

			//Log.d(TAG, "selRect=" + selRect);
			if(input!=""){
				// Draw the selection...
				Paint selected = new Paint();
				selected.setColor(getResources().getColor(
						R.color.puzzle_selected));
				canvas.drawRect(selRect, selected);
			}
			if(input.length()>1){
				// Draw connecting lines
				Paint trace = new Paint();
				trace.setColor(getResources().getColor(R.color.boggle_arrow));
				trace.setStyle(Paint.Style.FILL);
				arrow.moveTo(arrowP1.x, arrowP1.y);
				arrow.lineTo(arrowP2.x, arrowP2.y);
				arrow.lineTo(arrowP3.x, arrowP3.y);
				canvas.drawPath(arrow,trace);
			}
		}
		//End of game score screen
		if(time==0||gameStatus==STOPPED){
			Paint timeUp = new Paint();
			timeUp.setColor(Color.BLUE);
			timeUp.setAlpha(100);
			canvas.drawRect(0, height, 5*width, 6*height, timeUp);
			Paint timeUpTxt = new Paint(Paint.ANTI_ALIAS_FLAG);
			timeUpTxt.setColor(Color.RED);
			timeUpTxt.setTextSize(height*0.75f);
			timeUpTxt.setAlpha(200);
			timeUpTxt.setTextAlign(Paint.Align.CENTER);
			if(!oppDone)
				canvas.drawText(timeUpMsg, getWidth()/2, getHeight()/2, timeUpTxt);
			else{
				if(opponentScore == score)
					canvas.drawText("Tied!",getWidth()/2,getHeight()/2-height,timeUpTxt);
				else
					canvas.drawText("You "+(opponentScore>score? "LOSE!" : "WIN!"),getWidth()/2,getHeight()/2-height,timeUpTxt);
			}
			timeUpTxt.setColor(Color.WHITE);
			if(!oppDone)
				canvas.drawText(timeUpMsg, getWidth()/2+1, getHeight()/2+1, timeUpTxt);
			else{
				timeUpTxt.setTextSize(width*0.5f);
				canvas.drawText("Opponent Score: " + opponentScore, getWidth()/2, getHeight()/2, timeUpTxt);
				canvas.drawText("Personal Best: " + personalBest, getWidth()/2, getHeight()/2+height, timeUpTxt);
			}
		}
	}

	//Handle inputs
	@Override
	public boolean onTouchEvent(MotionEvent event) {

		if(gameStatus==STOPPED){
			//when game stops and result shows, only back button is functioning
			if(event.getAction() == MotionEvent.ACTION_DOWN &&
					event.getX()/width>1.5&&event.getX()/width<3.5&&event.getY()/height>6){
				Music.playOnce(getContext(), R.raw.button);
				backToBoggleMain();
				return true;
			}else 
				return super.onTouchEvent(event);
		}

		if (event.getAction() == MotionEvent.ACTION_DOWN){
			Log.d(TAG,"onTouchEvent: x " + (int)(event.getX()/width) + ", y " + (int)(event.getY()/height));
			if(isPaused) {
				if(inCircle(event.getX(),event.getY(),5*width/4,
						3*7*height/6,Math.min(5*width/4,7*height/6))){
					//resume after paused
					Music.playOnce(getContext(), R.raw.button);
					isPaused = false;
					gameStatus = RESUMED;
					BoggleSharedData.setGameStatus(RESUMED);
					Music.playBoggle(boggleGame, R.raw.bogglegame);
					invalidate();
					return true;
				} else if(inCircle(event.getX(),event.getY(),
						3*5*width/4,3*7*height/6,Math.min(5*width/4, 7*height/6))){
					//back when paused
					Music.playOnce(getContext(), R.raw.button);
					gameStatus = PAUSED;
					BoggleSharedData.setGameStatus(PAUSED);
					backToBoggleMain();
					return true;
				}else
					return super.onTouchEvent(event);
			}
			if(event.getY()/height<1){
				Music.playOnce(getContext(), R.raw.button);
				if(event.getX()/width<1){
					//pause when playing
					isPaused = true;
					gameStatus = PAUSED;
					BoggleSharedData.setGameStatus(PAUSED);
					Music.stop(boggleGame);
					invalidate();
					return true;
				}
				if(event.getX()/width>4){
					//back when playing
					gameStatus = RESUMED;
					BoggleSharedData.setGameStatus(RESUMED);
					backToBoggleMain();
					return true;
				}
			}
			if (event.getY() / height >= 1 && event.getY() / height <= 7) {
				//select letter by clicking
				dragging = false;
				select((int) (event.getX() / width),
						(int) (event.getY() / height));
			}
			if(event.getY()/height > 6) {
				if(event.getX()/width >4 && input.length()>2) {
					//submit current word
					onSubmitWord();
				} else if(event.getX()/width <1){
					//cancel current word
					Music.playOnce(getContext(), R.raw.button);
					onCancelWord();
				} else {
					return super.onTouchEvent(event);
				}
			}
			return true;
		} else if(event.getAction() == MotionEvent.ACTION_MOVE){

			Log.d(TAG, "onDrag()"+event.getX()+","+event.getY());
			if(event.getY()/height>1&&event.getY()/height<6){
				//select letter by dragging
				dragging = true;
				if(inSmallerRect(event.getX(),event.getY())){
					Log.d(TAG,"select on drag"+(int) (event.getX() / width)+","+
							(int) (event.getY() / height));
					select((int) (event.getX() / width),
							(int) (event.getY() / height));
				}
			}
			return true;
		} else if(event.getAction() == MotionEvent.ACTION_UP
				&& event.getY()/height>1 && event.getY()/height<6
				&& dragging){
			//done dragging, submit current word
			if(input.length()>2) onSubmitWord();
			else onCancelWord();
			dragging = false;
			return true;
		} else {
			return super.onTouchEvent(event);
		}
	}

	private boolean inSmallerRect(float x, float y) {
		float xRatio = (float)(x/width)-(int)(x/width);
		float yRatio = (float)(y/height) - (int)(y/height);
		return yRatio>0.25&&yRatio<0.75&&xRatio>0.25&&xRatio<0.75;
	}

	private void backToBoggleMain() {
		Intent back = new Intent(boggleGame, PersistentBoggle.class);
		boggleGame.startActivity(back);		
	}

	private void onCancelWord() {
		Log.d(TAG,"Cancel!");
		//Close the path
		arrow.reset();
		//clear input
		input="";
		//clear selected tiles
		selectedTiles = new ArrayList<Integer>();
		//Redraw the canvas
		invalidate();
	}

	private void onSubmitWord() {
		Log.d(TAG, "Submit!"+"input len:"+input.length());
		//Close the path
		if(input.length()<3) return;
		arrow.reset();

		//Calculate score
		if(duplicatedWord()) {
			Music.playOnce(getContext(), R.raw.duplicate);
		}
		else if(isWord(input)){
			Music.playOnce(getContext(), R.raw.right);
			score += input.length() - 2;
			addToUsedWords(input);
			tempWords = tempWords + input + ",";
			numWords++;
		}
		else {
			Music.playOnce(getContext(), R.raw.wrong);
		}

		//Clear input letters
		input = "";
		//Clear Selected Tiles
		selectedTiles = new ArrayList<Integer>();
		//Redraw the canvas
		invalidate();
	}

	private boolean inCircle(float x, float y, float cX, float cY, float radius) {
		return Math.sqrt((cX-x)*(cX-x)+(cY-y)*(cY-y))<=radius;
	}

	private void addToUsedWords(String word) {
		if(!usedWords.contains(word))
			usedWords.add(word);
	}

	private boolean duplicatedWord() {
		return usedWords.contains(input);
	}

	private void select(int x, int y) {
		invalidate();
		int selectX = Math.min(Math.max(x, 0), 4);
		int selectY = Math.min(Math.max(y, 1), 6) - 1;
		if(input==""){
			selX = selectX;
			selY =selectY;
		}else{
			if(Math.abs(selectX-selX) > 1 
					|| Math.abs(selectY - selY) >1
					|| (selectY == selY && selectX == selX))
				return;
			else if(wasSelected(selectX,selectY)) 
				return;
			else{
				arrowP1.x = (int) (selX*width+width/2);
				arrowP1.y = (int) ((selY+1)*height+height/2);
				arrowP2.x = (int) (selX*width+width/2);
				arrowP2.y = (int) ((selY+1)*height+height/2);
				arrowP3.x = (int) (selectX*width+width/2);
				arrowP3.y = (int) ((selectY+1)*height+height/2);
				if(selY<selectY) {
					arrowP1.x+=width/10;
					arrowP2.x-=width/10;
				}
				if(selY>selectY){
					arrowP1.x-=width/10;
					arrowP2.x+=width/10;
				}
				if(selX<selectX) {
					arrowP1.y-=height/10;
					arrowP2.y+=height/10;
				}
				if(selX>selectX){
					arrowP1.y+=height/10;
					arrowP2.y-=height/10;
				}
				//invalidate();
				selX = selectX;
				selY =selectY;
			}
		}
		getRect(selX, selY, selRect);
		selectedTiles.add(selY*5+selX);
		Music.playOnce(getContext(), R.raw.select);
		invalidate();
		input += boggleGame.getTileString(selX, selY);
		Log.d(TAG, "input ="+input);
		Log.d(TAG, "selRect=" + selRect);
	}

	private boolean wasSelected(int x, int y){
		return selectedTiles.contains(5*y+x);
	}

	private boolean isWord(String word) {
		try {
			String fileName = findFile(word);
			Log.d(TAG, "File name is:"+fileName);
			int fileId = getResources().getIdentifier(fileName, "raw", getContext().getPackageName());
			if(fileId==0) {
				Log.d(TAG,"File Not Found.");
				return false;
			}
			InputStream wordList = getResources().openRawResource(fileId);
			BufferedReader reader = new BufferedReader(new InputStreamReader(wordList));
			String line;
			while((line = reader.readLine())!=null){
				if(line.equalsIgnoreCase(word)) {
					wordList.close();
					return true;
				}
			}
			wordList.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	private String findFile(String word) {
		String fileName = "wordlist"+ word.charAt(0)+word.charAt(1);
		fileName = fileName.toLowerCase();
		return fileName;
	}

}
