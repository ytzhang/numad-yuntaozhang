package edu.neu.madcourse.yuntaozhang.boggle.persistent;

import java.util.ArrayList;

import edu.neu.madcourse.yuntaozhang.boggle.BogglePuzzleView;

public class BoggleSharedData {
	private static int boggleGameStatus = BogglePuzzleView.STOPPED;
	private static ArrayList<String> usedWords = new ArrayList<String>();
	private static boolean boggleMusicPref = true;
	private static String hostPhone = "";
	private static String opponentPhone = "";
	public final static String teamName = "Yuntao";
	public final static String password = "1234";
	public final static String error = "ERROR";
	public final static String words = "words";
	public final static String status = "Status";
	public final static String TOP = "top";
	public final static String TIME_TOP = "Time for Top";
	public final static String ID_TOP = "Phone Number for Top";
	
	public static void setGameStatus(int status){
		boggleGameStatus = status;
	}
	public static int getGameStatus(){
		return boggleGameStatus;
	}
	public static ArrayList<String> getUsedWords() {
		return usedWords;
	}
	public static void setUsedWords(ArrayList<String> usedWords) {
		BoggleSharedData.usedWords = usedWords;
	}
	public static boolean getBoggleMusicPref() {
		return boggleMusicPref;
	}
	public static void setBoggleMusicPref(boolean musicPref) {
		boggleMusicPref = musicPref;
	}
	public static String getHostPhone() {
		return hostPhone;
	}
	public static void setHostPhone(String hostPhone) {
		BoggleSharedData.hostPhone = hostPhone;
	}
	public static String getOpponentPhone() {
		return opponentPhone;
	}
	public static void setOpponentPhone(String opponentPhone) {
		BoggleSharedData.opponentPhone = opponentPhone;
	}

}
