package edu.neu.madcourse.yuntaozhang.boggle.persistent;

import java.util.Random;

import edu.neu.madcourse.yuntaozhang.Music;
import edu.neu.madcourse.yuntaozhang.R;
import edu.neu.madcourse.yuntaozhang.R.layout;
import edu.neu.madcourse.yuntaozhang.R.menu;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import edu.neu.mobileclass.apis.KeyValueAPI;

public class BoggleGame extends Activity {
	private static final String TAG = "Boggle";

	private BogglePuzzleView bogglePuzzleView;
	private char puzzle[] = new char[5*5];

	public static final int DIFFICULTY_EASY = 0;
	public static final int DIFFICULTY_CONTINUE = -1;

	public static final String KEY_DIFFICULTY = 
			"edu.neu.madcourse.yuntaozhang.difficulty";

	private final String PREF_PUZZLE  = "prefPuzzle";

	private int gameDiff = DIFFICULTY_EASY;

	private final String easyPuzzle = 
			"AAAAAABBCCCDDDEEEEEEFFGGGHHHIIIJKKLLL"
					+"MMMNNNOOOOPPPQRRRSSSTTTTUUUUVVWWXXYYZZ";
	public boolean inGame = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		gameDiff = getIntent().getIntExtra(KEY_DIFFICULTY,
				DIFFICULTY_EASY);
		inGame = true;
		if(gameDiff==DIFFICULTY_CONTINUE)
			puzzle = getPuzzle(gameDiff);
		else if(getIntent().getStringExtra(FindMatch.PUZ).length()!=25){
			puzzle = getPuzzle(gameDiff);
			new syncPuzzleTask().execute();
		}
		else{
			puzzle = getIntent().getStringExtra(FindMatch.PUZ).toCharArray();
		}
		bogglePuzzleView = new BogglePuzzleView(this);
		setContentView(bogglePuzzleView);
		bogglePuzzleView.requestFocus();
	}
	
	private class syncPuzzleTask extends AsyncTask<String, String, String>{

		@Override
		protected String doInBackground(String... params) {
			String puzzleStr = new String(puzzle);
			// save the current puzzle to server
			while(KeyValueAPI.put(BoggleSharedData.teamName, BoggleSharedData.password, 
					FindMatch.PUZ+BoggleSharedData.getHostPhone(), puzzleStr)
				.contains(BoggleSharedData.error));
			return null;
		}
		
	}
	
	public void pushNotice(String word){
		if(inGame) return;
		NotificationCompat.Builder mBuilder =
		        new NotificationCompat.Builder(this)
		        .setSmallIcon(R.drawable.sunflower_icon)
		        .setContentTitle("Your opponent made a move!")
		        .setContentText(word+" is found!");
		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(this, PersistentBoggle.class);

		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(PersistentBoggle.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(
		            0,
		            PendingIntent.FLAG_UPDATE_CURRENT
		        );
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager =
		    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		int mId = 1;
		mNotificationManager.notify(mId, mBuilder.build());
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Music.playBoggle(this, R.raw.bogglegame);
		inGame = true;
		if(BoggleSharedData.getGameStatus()==BogglePuzzleView.STOPPED)
			gameDiff = DIFFICULTY_EASY;
		if(gameDiff==DIFFICULTY_CONTINUE)
			bogglePuzzleView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause");
		Music.stop(this);
		inGame = false;
		// Save the current puzzle
		bogglePuzzleView.onPause();
		getPreferences(MODE_PRIVATE).edit().putString("MYPUZZLE", toPuzzleString()).commit();	      
	}


	private String toPuzzleString() {
		String currentPuzzle = "";
		for(char letter : puzzle)
			currentPuzzle+=letter;
		return currentPuzzle;
	}
	/** Given a difficulty level, come up with a new puzzle */
	private char[] getPuzzle(int diff) {
		String puz;
		switch (diff) {
		case DIFFICULTY_CONTINUE:
			puz = getPreferences(MODE_PRIVATE).getString("MYPUZZLE",
					easyPuzzle);
			break;
		case DIFFICULTY_EASY:
			puz = easyPuzzle;
			break;
		default:
			puz = easyPuzzle;
			break;
		}
		return fromPuzzleString(puz,diff);
	}

	private char[] fromPuzzleString(String puz, int diff) {
		int i =0;
		char[] newPuz = new char[5*5];
		if(diff==DIFFICULTY_CONTINUE){
			for(i=0; i<5*5; i++)
				newPuz[i]=puz.charAt(i);
		} else {
			Random random = new Random();
			for(i = 0; i<5*5; i++){
				newPuz[i] = puz.charAt(random.nextInt(puz.length()));
			}
		}
		return newPuz;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_boggle_game, menu);
		return true;
	}

	/** Return the tile at the given coordinates */
	private char getTile(int x, int y) {
		return puzzle[y * 5 + x];
	}

	/** Change the tile at the given coordinates */
	private void setTile(int x, int y, char value) {
		puzzle[y * 5 + x] = value;
	}

	/** Return a string for the tile at the given coordinates */
	protected String getTileString(int x, int y) {
		char v = getTile(x, y);
		if (v == 0)
			return "";
		else
			return String.valueOf(v);
	}

	/** Return the current puzzle char array*/
	protected char[] getPuzzleChars(){
		return this.puzzle;
	}

	/** Set the current puzzle car array with the give chars*/
	protected void setPuzzleChars(char[] newPuzzle){
		this.puzzle=newPuzzle;
	}

}
