package edu.neu.madcourse.yuntaozhang.boggle.persistent;

import edu.neu.madcourse.yuntaozhang.R;
import edu.neu.madcourse.yuntaozhang.R.layout;
import edu.neu.madcourse.yuntaozhang.R.menu;
import edu.neu.mobileclass.apis.KeyValueAPI;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

public class TopList extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.boggle_top_list);
		if(isConnected()){
			new topScoreTask().execute(null);
		}
	}
	
	private boolean isConnected() {
		// check if the phone have internet connection and the server is available
		ConnectivityManager connMgr = (ConnectivityManager) 
				getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			if(!KeyValueAPI.isServerAvailable())
				Toast.makeText(getApplicationContext(), "Remote server is not available!", 
						Toast.LENGTH_LONG).show();
			else{
				Toast.makeText(getApplicationContext(), "Network is available!", 
						Toast.LENGTH_SHORT).show();
				return true;
			}

		} else {
			Toast.makeText(getApplicationContext(), "Network is not available!", 
					Toast.LENGTH_LONG).show();
		}
		return false;
	}
	
	private class topScoreTask extends AsyncTask<String, String, String>{
		private final String teamName = BoggleSharedData.teamName;
		private final String password = BoggleSharedData.password;
		private final String error = BoggleSharedData.error;
		private final String TOP = BoggleSharedData.TOP;
		private final String TIME_TOP = BoggleSharedData.TIME_TOP;
		private final String ID_TOP = BoggleSharedData.ID_TOP;
		private int rank = 1;

		@Override
		protected String doInBackground(String... params) {
			//Fetch the top 5 score list from server
			String time = "";
			String score = "";
			String phone = "";
			for(rank=1; rank<=5; rank++){
				while((phone = KeyValueAPI.get(teamName, password, ID_TOP+rank))
						.contains(error));
				while((time = KeyValueAPI.get(teamName, password, TIME_TOP+rank))
						.contains(error));
				while((score = KeyValueAPI.get(teamName, password, TOP+rank))
						.contains(error));
				publishProgress(phone+"      "+score+"      "+time);
			}
			
			/*for(int i =1; i<=5; i++){
			KeyValueAPI.clearKey(teamName, password, TOP+i);
			KeyValueAPI.clearKey(teamName, password, TIME_TOP+i);
			KeyValueAPI.clearKey(teamName, password, ID_TOP+i);
			}*/
			return null;
		}
		
		@Override
		protected void onProgressUpdate(String... progress){
			TextView tv = null;
			switch(rank){
			case 1:
				tv = (TextView)findViewById(R.id.boggle_top1);
				break;
			case 2:
				tv = (TextView)findViewById(R.id.boggle_top2);
				break;
			case 3:
				tv = (TextView)findViewById(R.id.boggle_top3);
				break;
			case 4:
				tv = (TextView)findViewById(R.id.boggle_top4);
				break;
			case 5:
				tv = (TextView)findViewById(R.id.boggle_top5);
				break;
			default:
				break;
			}
			if(tv!=null)
				tv.setText(" " + rank + "         " + progress[0]);
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.boggle_top_list, menu);
		return true;
	}

}
