package edu.neu.madcourse.yuntaozhang.boggle.persistent;

import edu.neu.madcourse.yuntaozhang.HelloMAD;
import edu.neu.madcourse.yuntaozhang.Music;
import edu.neu.madcourse.yuntaozhang.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

public class PersistentBoggle extends Activity implements OnClickListener{
	private static final String TAG = "Boggle";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.boggle_persistent);
		int onGame = BoggleSharedData.getGameStatus();
		View continueButton = findViewById(R.id.continue_boggle_button);
		View newGameButton = findViewById(R.id.new_boggle_button);
		if(onGame!=BogglePuzzleView.RESUMED
				&&onGame!=BogglePuzzleView.PAUSED){
			continueButton.setVisibility(View.GONE);
			newGameButton.setOnClickListener(this);
			}
		else{
			newGameButton.setVisibility(View.GONE);
			continueButton.setOnClickListener(this);
		}
		View acknowledgementsButton = findViewById(R.id.acknowledgements_boggle_button);
		acknowledgementsButton.setOnClickListener(this);
		
		View exitButton = findViewById(R.id.exit_boggle_button);
		exitButton.setOnClickListener(this);
		
		View topList = findViewById(R.id.top_boggle_button);
		topList.setOnClickListener(this);
		
		View instructionButton = findViewById(R.id.instructions_boggle_button);
		instructionButton.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_boggle, menu);
		return true;
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		Music.playBoggle(this, R.raw.boggle);
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		Music.stop(this);
	}

	public void onClick(View v) {
		switch(v.getId()){
		case R.id.acknowledgements_boggle_button:
			Intent acknowledgements = new Intent(this, BoggleAcknowledgements.class);
			startActivity(acknowledgements);
			break;
		case R.id.instructions_boggle_button:
			Intent instruction = new Intent(this, Instructions.class);
			startActivity(instruction);
			break;
		case R.id.top_boggle_button:
			Intent topScore = new Intent(this, TopList.class);
			startActivity(topScore);
			break;
		case R.id.continue_boggle_button:
			Intent continueGame = new Intent(this, BoggleGame.class);
			continueGame.putExtra(BoggleGame.KEY_DIFFICULTY, BoggleGame.DIFFICULTY_CONTINUE);
		      startActivity(continueGame);
		      break;
		case R.id.new_boggle_button:
			Intent findMatch = new Intent(this, FindMatch.class);
			startActivity(findMatch);
			break;
		case R.id.exit_boggle_button:
			Intent backToMain = new Intent(this, HelloMAD.class);
			startActivity(backToMain);
			break;
		}		
	}
	
}
