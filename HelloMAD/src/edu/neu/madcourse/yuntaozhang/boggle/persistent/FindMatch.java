package edu.neu.madcourse.yuntaozhang.boggle.persistent;

import edu.neu.madcourse.yuntaozhang.R;
import edu.neu.madcourse.yuntaozhang.R.layout;
import edu.neu.madcourse.yuntaozhang.R.menu;
import edu.neu.mobileclass.apis.KeyValueAPI;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FindMatch extends Activity implements OnClickListener{

	private final static String TAG = "Boggle_FindMatch";
	public final static String PUZ = "PuzzleFrom";
	private String errorMsg = "";
	private final static String teamName = BoggleSharedData.teamName;
	private final static String password = BoggleSharedData.password;
	private final static String error = BoggleSharedData.error;
	private String opponentPhone = "";
	private String phoneNumber = "";
	private String puzzle = "";
	private boolean canceled = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.boggle_find_match);
		View submitButton = findViewById(R.id.submit_boggle_button);
		submitButton.setOnClickListener(this);
		View cancelButton = findViewById(R.id.cancel_boggle_button);
		cancelButton.setOnClickListener(this);
	}

	private boolean isConnected() {
		// check if the phone have internet connection and the server is available
		ConnectivityManager connMgr = (ConnectivityManager) 
				getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			if(!KeyValueAPI.isServerAvailable())
				Toast.makeText(getApplicationContext(), "Remote server is not available!", 
						Toast.LENGTH_LONG).show();
			else{
				Toast.makeText(getApplicationContext(), "Network is available!", 
						Toast.LENGTH_SHORT).show();
				return true;
			}

		} else {
			Toast.makeText(getApplicationContext(), "Network is not available!", 
					Toast.LENGTH_LONG).show();
		}
		return false;
	}

	private void errorHandler() {


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.boggle_find_match, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.submit_boggle_button:
			submitPhoneNumber();
			break;
		case R.id.cancel_boggle_button:
			canceled = true;
			finish();
			break;
		default:
			finish();
			break;
		}
	}

	private void submitPhoneNumber() {
		EditText editTextView = (EditText)findViewById(R.id.inputPhoneNumber); 
		opponentPhone = editTextView.getText().toString();
		if(opponentPhone.length()<=0)
			return;
		if(!isConnected()){
			errorHandler();
			return;
		}
		new findMatchTask().execute(null);
	}

	private class findMatchTask extends AsyncTask<String, String, String>{
		private final static String REGISTER = "Registering...";
		private final static String CONNECTING = "Connecting ...";
		private final static String START = "start";
		private final static String BUSY = "busy";
		private final static String PENDING = "pending";
		private final static String PUZZLE = "puzzle";
		private final static String STOPPED = "stopped";

		private String findMatchByPhoneNumber() {
			// find the opponent by phone number
			String opponentValue = KeyValueAPI.get(teamName, password, opponentPhone);
			if(opponentValue.equalsIgnoreCase(phoneNumber)){
				return START;
			} else if(opponentValue.length()<=0){
				return PENDING;
			} else {
				return BUSY;
			}
		}
		
		private void clearKeys() {
			// clear key created by this match
			String result = "ERROR";
			while(result.equalsIgnoreCase(error))
				result = KeyValueAPI.clearKey(teamName, password, phoneNumber)
				+ KeyValueAPI.clearKey(teamName, password, BoggleSharedData.words+phoneNumber)
				+ KeyValueAPI.clearKey(teamName, password, FindMatch.PUZ+phoneNumber);
		}

		@Override
		protected String doInBackground(String... urls) {
			register();
			String status = findMatchByPhoneNumber();
			if(status.equalsIgnoreCase(PENDING))
				return waitForMatch();
			if(status.equalsIgnoreCase(BUSY))
				abortMatch();
			return status;
		}

		private void abortMatch() {
			//abort match because opponent is busy
			while(KeyValueAPI.clearKey(teamName, password, phoneNumber).contains(error));
		}

		private String waitForMatch() {
			//sit in a loop to get the puzzle created by opponent
			int attempt = 200;
			while((puzzle=KeyValueAPI.get(teamName, password, PUZ+opponentPhone)).length()<=0){
				if(puzzle.contains(error)||attempt<=0){
					abortMatch();
					return PENDING;
				} 
				if(attempt%25==0){	
					publishProgress(CONNECTING);
				}
				if(canceled){
					clearKeys();
					return STOPPED;
				}
				attempt--;
			}
			return START;
		}

		private void register() {
			//register that player A want to play with B using their phone numbers
			while(KeyValueAPI.put(teamName, password, phoneNumber, opponentPhone).contains(error))
				publishProgress(REGISTER);
		}

		@Override
		protected void onProgressUpdate(String... progress){
			if(progress[0].equalsIgnoreCase(REGISTER))
				Toast.makeText(getApplicationContext(), REGISTER, Toast.LENGTH_SHORT).show();
			if(progress[0].equalsIgnoreCase(CONNECTING))
				Toast.makeText(getApplicationContext(), CONNECTING, Toast.LENGTH_SHORT).show();
		}

		@Override
		protected void onPreExecute(){
			//Get the phone number
			TelephonyManager tMgr =(TelephonyManager)getApplicationContext().
					getSystemService(Context.TELEPHONY_SERVICE);
			phoneNumber =  tMgr.getLine1Number().substring(1);
			//display phone number
			TextView tv =(TextView) findViewById(R.id.local_phone_number_text);
			tv.setText(phoneNumber);
			//disable submit button
			findViewById(R.id.submit_boggle_button).setEnabled(false);
		}

		private void sleep(int sec){
			try {
				Thread.sleep(sec*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		protected void onPostExecute(String result){
			if(result.equals(BUSY))
				Toast.makeText(getApplicationContext(), "Player "+opponentPhone+"is busy now!", 
						Toast.LENGTH_LONG).show();
			else if(result.equals(START)){
				Toast.makeText(getApplicationContext(), "Connected with "+opponentPhone+"!", 
						Toast.LENGTH_LONG).show();
				startNewGame();
			}
			else if(result.equals(STOPPED)){
				return;
			}
			else
				Toast.makeText(getApplicationContext(), "An unexpected error occured connecting with "
						+opponentPhone+"!\nPlease try again later!", Toast.LENGTH_LONG).show();
			//enable submit button
			findViewById(R.id.submit_boggle_button).setEnabled(true);
		}
	}

	public void startNewGame() {
		//Set host and guest phone number
		BoggleSharedData.setHostPhone(phoneNumber);
		BoggleSharedData.setOpponentPhone(opponentPhone);
		// when connection is established, start a new boggle game
		Intent newGame = new Intent(this, BoggleGame.class);
		newGame.putExtra(PUZ, puzzle);
		startActivity(newGame);
	}

}
