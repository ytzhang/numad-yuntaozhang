package edu.neu.madcourse.yuntaozhang.boggle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import edu.neu.madcourse.yuntaozhang.Music;
import edu.neu.madcourse.yuntaozhang.R;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class BogglePuzzleView extends View {

	private static final String TAG = "Boggle";
	private final BoggleGame boggleGame;
	private int ID = 43;



	public int gameStatus=RESUMED;
	public static int STOPPED = 0;
	public static int RESUMED = 1;
	public static int PAUSED = 2;
	private boolean isPaused = false;
	private String pauseMsg = "Paused";
	private String timeUpMsg = "Time's Up!";

	private float width;    // width of one tile
	private float height;   // height of one tile
	private int selX;       // X index of selection
	private int selY;       // Y index of selection
	private final Rect selRect = new Rect();

	private ArrayList<Integer> selectedTiles= new ArrayList<Integer>();

	private final Path arrow = new Path();
	private final Point arrowP1 = new Point();
	private final Point arrowP2 = new Point();
	private final Point arrowP3 = new Point();	
	private int score = 0;
	private int time = 180;

	protected void setTime(int newTime){
		time=newTime;
	}

	protected void setScore(int newScore){
		score = newScore;
	}

	Timer timer = new Timer();

	public static String TIME = "time";
	public static String SCORE = "score";
	public static String STATUS = "status";
	public static String GAME_BOARD = "gameBoard";
	public static String USED_WORDS = "usedWords";

	private static final String VIEW_STATE = "viewState";

	private final Handler countDownHandler = new Handler();
	private final Runnable startCountDown = new Runnable(){
		public void run(){
			if(time<=0||gameStatus==STOPPED) {
				timer.cancel();
				gameStatus = STOPPED;
				BoggleSharedData.setGameStatus(STOPPED);
				Music.stop(boggleGame);
				invalidate();
			}
			else invalidate();
		}
	};
	TimerTask countDown = new TimerTask(){
		@Override
		public void run(){
			countDown();
		}
	};
	
	private void countDown() {
		if(isPaused) return;
		time--;
		countDownHandler.post(startCountDown);
	}

	private String input = "";
	private boolean dragging = false;
	private ArrayList<String> usedWords = new ArrayList<String>();
	private int numWords = 0;


	public BogglePuzzleView(Context context) {
		super(context);
		this.boggleGame = (BoggleGame) context;
		setFocusable(true);
		setFocusableInTouchMode(true);
		if(gameStatus!=RESUMED||gameStatus!=PAUSED)
			timer.schedule(countDown, 0, 1000);
		setId(ID); 
	}

	@Override
	protected Parcelable onSaveInstanceState() { 
		Parcelable p = super.onSaveInstanceState();
		Log.d(TAG, "onSaveInstanceState");
		Bundle bundle = new Bundle();
		bundle.putInt(TIME, time);
		bundle.putInt(SCORE, score);
		bundle.putInt(STATUS, gameStatus);
		bundle.putCharArray(GAME_BOARD, boggleGame.getPuzzleChars());
		bundle.putStringArrayList(USED_WORDS, usedWords);
		bundle.putParcelable(VIEW_STATE, p);
		return bundle;
	}
	@Override
	protected void onRestoreInstanceState(Parcelable state) { 
		Log.d(TAG, "onRestoreInstanceState");
		Bundle bundle = (Bundle) state;
		time = bundle.getInt(TIME);
		score = bundle.getInt(SCORE);
		boggleGame.setPuzzleChars(bundle.getCharArray(GAME_BOARD));
		gameStatus= bundle.getInt(STATUS);
		usedWords = bundle.getStringArrayList(USED_WORDS);
		super.onRestoreInstanceState(bundle.getParcelable(VIEW_STATE));
	}
	public void onPause(){
		Log.d(TAG,"onPause()");
		boggleGame.getPreferences(Context.MODE_PRIVATE).edit().putInt(TIME, time).commit();
		boggleGame.getPreferences(Context.MODE_PRIVATE).edit().putInt(SCORE, score).commit();
		BoggleSharedData.setUsedWords(usedWords);

	}
	public void onResume(){
		Log.d(TAG,"onResume()");
		time = boggleGame.getPreferences(Context.MODE_PRIVATE).getInt(TIME, time);
		score = boggleGame.getPreferences(Context.MODE_PRIVATE).getInt(SCORE, score);
		usedWords=BoggleSharedData.getUsedWords();
		gameStatus = BoggleSharedData.getGameStatus();
		if(time<=0||gameStatus==STOPPED) {
			Intent newGame = new Intent(boggleGame, BoggleGame.class);
			boggleGame.startActivity(newGame);
		}else{
			if(gameStatus==PAUSED) isPaused = true;
			else isPaused = false;
		}
	}
	



	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh){
		width = w / 4f;
		height = h /6f;
		getRect(selX, selY, selRect);
	}

	private void getRect(int x, int y, Rect rect) {
		rect.set((int)(x*width),(int)(1*height + y*height),
				(int)(x*width+width),(int)(y*height+2*height));	
	}

	@Override
	protected void onDraw(Canvas canvas){
		//Draw the background
		Paint backgraound = new Paint();
		backgraound.setColor(getResources().
				getColor(R.color.puzzle_background));
		canvas.drawRect(0, 0, getWidth(), getHeight(), backgraound);

		//Draw the game board
		//Define colors for the grid lines
		Paint hilite = new Paint();
		hilite.setColor(getResources().getColor(R.color.puzzle_hilite));
		Paint light = new Paint();
		light.setColor(getResources().getColor(R.color.puzzle_light));

		//Draw the minor grid lines
		for(int i=0; i < 5 ; i ++){
			canvas.drawLine(0, i*height+1*height+1, width*4, i*height+1*height+1, hilite);
			canvas.drawLine(i*width+1, 0+1*height, i*width+1, height*4+1*height, hilite);
			canvas.drawLine(0, i*height+1*height, width*4, i*height+1*height, light);
			canvas.drawLine(i*width, 0+1*height, i*width, height*4+1*height, light);
		}

		if(isPaused){
			//Resume Button
			Paint buttonCircle = new Paint();
			buttonCircle.setColor(getResources().getColor(R.color.boggle_resume_circle));
			float cX = getWidth()/4;
			float cY = getHeight()/2;
			float r = Math.min(width, height);
			canvas.drawCircle(cX, cY, r, buttonCircle);
			Paint mark = new Paint();
			mark.setColor(Color.LTGRAY);
			mark.setStyle(Paint.Style.STROKE);
			mark.setStrokeWidth(5);
			Path markPath = new Path();
			float[] resumeLines = {cX-r/3,cY-r/3,cX+r/3,cY,cX-r/3,cY+r/3,cX-r/3,cY-r/3 };
			markPath.moveTo(resumeLines[0], resumeLines[1]);
			markPath.lineTo(resumeLines[2], resumeLines[3]);
			markPath.lineTo(resumeLines[4], resumeLines[5]);
			markPath.lineTo(resumeLines[0], resumeLines[1]);
			//canvas.drawPath(markPath, mark);
			//Back Button
			cX = getWidth()*3/4;
			canvas.drawCircle(cX, cY, r, buttonCircle);
			markPath.moveTo(cX-2*r/3, cY);
			markPath.lineTo(cX-r/3, cY-r/3);
			markPath.lineTo(cX-r/3, cY-r/6);
			markPath.lineTo(cX+2*r/3, cY-r/6);
			markPath.lineTo(cX+2*r/3, cY+r/6);
			markPath.lineTo(cX-r/3, cY+r/6);
			markPath.lineTo(cX-r/3, cY+r/3);
			markPath.lineTo(cX-2*r/3, cY);
			markPath.close();
			canvas.drawPath(markPath, mark);
			//Pause Message
			Paint pausePaint = new Paint(Paint.ANTI_ALIAS_FLAG); 
			pausePaint.setColor(Color.BLACK);
			pausePaint.setStyle(Style.FILL);
			pausePaint.setTextSize(height * 0.75f);
			canvas.drawText(pauseMsg, width, height, pausePaint);
			pausePaint.setColor(Color.LTGRAY);
			canvas.drawText(pauseMsg, width+1, height+1, pausePaint);
		} else {
			//Pause/Submit word/cancel word Buttons
			if(gameStatus!=STOPPED){
				Paint buttons = new Paint();
				buttons.setColor(getResources().getColor(R.color.boggle_resume_circle));
				//Pause Button
				canvas.drawRect(5, 5, width-5, height-5, buttons);
				if(input.length()>2&&!dragging){
				//submit button
				canvas.drawRect(3*width+5, 5*height+5, 4*width-5, 6*height-5, buttons);
				}
				if(input.length()>0&&!dragging){
				//cancel button
				canvas.drawRect(5, 5*height+5, width-5, 6*height-5, buttons);
				}
				
				buttons.setColor(Color.LTGRAY);
				//pause mark
				canvas.drawRect(width/6+width/8, height/3, width/3+width/8, height*2/3, buttons);
				canvas.drawRect(width*4/6-width/8, height/3, width*5/6-width/8, height*2/3, buttons);
				
				Paint wordPaint = new Paint(Paint.ANTI_ALIAS_FLAG); 
				wordPaint.setStyle(Style.FILL);
				wordPaint.setTextSize(Math.min(height * 0.75f,width/(input.length()+1)));
				wordPaint.setColor(Color.BLACK);
				wordPaint.setTextAlign(Paint.Align.CENTER);
				canvas.drawText(input, 2*width, (int)(height*5.7), wordPaint);
				
				
				buttons.setStyle(Style.STROKE);
				buttons.setStrokeWidth(5);
				if(input.length()>2&&!dragging){
				//submit mark
				buttons.setColor(Color.RED);
				buttons.setAlpha(100);
				canvas.drawLine(3*width+width/3, 5*height+height/2, 3*width+width/2, 5*height+height*3/4, buttons);
				canvas.drawLine(3*width+width/2, 5*height+height*3/4, 3*width+width*3/4, 5*height+height/4, buttons);
				}
				if(input.length()>0&&!dragging){
				//cancel mark
				buttons.setColor(Color.BLUE);
				buttons.setAlpha(100);
				canvas.drawLine(width/4, 5*height+height/4, width*3/4, 5*height+height*3/4, buttons);
				canvas.drawLine(width/4, 5*height+height*3/4, width*3/4, 5*height+height/4, buttons);
				}
			}

			//Back Button
			Paint backButton = new Paint();
			backButton.setColor(getResources().getColor(R.color.boggle_resume_circle));
			Paint backArrow = new Paint();
			backArrow.setColor(Color.LTGRAY);
			backArrow.setStyle(Style.STROKE);
			backArrow.setStrokeWidth(5);
			float backX = 3*width;
			float backY = 0;
			float backWidth = width;
			if(gameStatus==STOPPED||time<=0) {
				//Relocate the position of back button when game over
				backX=width;
				backY=5*height;
				backWidth = 2*width;
			}
			canvas.drawRect(backX+5, backY+5, backX+backWidth-5, backY+height-5, backButton);
			canvas.drawLine(backX+backWidth/4-2, backY+height/2, backX+backWidth*3/4, backY+height/2, backArrow);
			canvas.drawLine(backX+backWidth/4, backY+height/2, backX+backWidth*3/8, backY+height/3, backArrow);
			canvas.drawLine(backX+backWidth/4, backY+height/2, backX+backWidth*3/8, backY+height*2/3, backArrow);


			// Define color and style for numbers
			Paint foreground = new Paint(Paint.ANTI_ALIAS_FLAG); 
			foreground.setColor(getResources().getColor(R.color.puzzle_foreground));
			foreground.setStyle(Style.FILL);
			foreground.setTextSize(Math.min(height,width) * 0.75f);
			//foreground.setTextScaleX(width / height);
			foreground.setTextAlign(Paint.Align.CENTER);
			// Draw the number in the center of the tile
			FontMetrics fm = foreground.getFontMetrics();
			// Centering in X: use alignment (and X at midpoint)
			float x = width / 2;
			// Centering in Y: measure ascent/descent first 
			float y = height / 2 - (fm.ascent + fm.descent) / 2;



			//Draw letters
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					canvas.drawText(this.boggleGame.getTileString(i, j), 
							i * width + x, j * height + 1*height + y, foreground);
				}
			}	
			//Draw the Timer
			if(gameStatus!=STOPPED){
				char sec1 = time%60>9?Integer.toString(time%60).charAt(0):'0';
				char sec2 = sec1=='0'?Integer.toString(time%60).charAt(0):Integer.toString(time%60%10).charAt(0);
				foreground.setTextSize(height*0.35f);
				canvas.drawText("0"+Integer.toString(time/60)+":"+sec1+sec2, width+2*x, y/2, foreground);
			}


			//Draw the Score board
			String scoreTxt = Integer.toString(score);
			foreground.setTextSize(height*0.38f);
			if(gameStatus==STOPPED) {
				foreground.setTextSize(height*0.75f);
				scoreTxt = "Score: "+scoreTxt;
			}
			canvas.drawText(scoreTxt, width+2*x, y, foreground);


			//Log.d(TAG, "selRect=" + selRect);
			if(input!=""){
				// Draw the selection...
				Paint selected = new Paint();
				selected.setColor(getResources().getColor(
						R.color.puzzle_selected));
				canvas.drawRect(selRect, selected);
			}
			if(input.length()>1){
				// Draw connecting lines
				Paint trace = new Paint();
				trace.setColor(getResources().getColor(R.color.boggle_arrow));
				trace.setStyle(Paint.Style.FILL);
				arrow.moveTo(arrowP1.x, arrowP1.y);
				arrow.lineTo(arrowP2.x, arrowP2.y);
				arrow.lineTo(arrowP3.x, arrowP3.y);
				canvas.drawPath(arrow,trace);
			}
		}
		if(time==0||gameStatus==STOPPED){
			Paint timeUp = new Paint();
			timeUp.setColor(Color.BLUE);
			timeUp.setAlpha(100);
			canvas.drawRect(0, height, 4*width, 5*height, timeUp);
			Paint timeUpTxt = new Paint(Paint.ANTI_ALIAS_FLAG);
			timeUpTxt.setColor(Color.RED);
			timeUpTxt.setTextSize(height*0.75f);
			timeUpTxt.setAlpha(200);
			timeUpTxt.setTextAlign(Paint.Align.CENTER);
			canvas.drawText(timeUpMsg, getWidth()/2, getHeight()/2, timeUpTxt);
			timeUpTxt.setColor(Color.WHITE);
			canvas.drawText(timeUpMsg, getWidth()/2+1, getHeight()/2+1, timeUpTxt);
		}
	}

	//Handle inputs
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN){
		Log.d(TAG,"onTouchEvent: x " + (int)(event.getX()/width) + ", y " + (int)(event.getY()/height));
		if(gameStatus==STOPPED){
			if(event.getX()/width>1&&event.getX()/width<3&&event.getY()/height>5){
				Music.playOnce(getContext(), R.raw.button);
				backToBoggleMain();
				return true;
			}else 
				return super.onTouchEvent(event);
		}
		if(isPaused) {
			if(inCircle(event.getX(),event.getY(),width,
					3*height,Math.min(width,height))){
				Music.playOnce(getContext(), R.raw.button);
				isPaused = false;
				gameStatus = RESUMED;
				BoggleSharedData.setGameStatus(RESUMED);
				Music.playBoggle(boggleGame, R.raw.bogglegame);
				invalidate();
				return true;
			} else if(inCircle(event.getX(),event.getY(),
					3*width,3*height,Math.min(width, height))){
				Music.playOnce(getContext(), R.raw.button);
				gameStatus = PAUSED;
				BoggleSharedData.setGameStatus(PAUSED);
				backToBoggleMain();
				return true;
			}else
				return super.onTouchEvent(event);
		}
		if(event.getY()/height<1){
			Music.playOnce(getContext(), R.raw.button);
			if(event.getX()/width<1){
				isPaused = true;
				gameStatus = PAUSED;
				BoggleSharedData.setGameStatus(PAUSED);
				Music.stop(boggleGame);
				invalidate();
				return true;
			}
			if(event.getX()/width>3){
				gameStatus = RESUMED;
				BoggleSharedData.setGameStatus(RESUMED);
				backToBoggleMain();
				return true;
			}
		}
		if (event.getY() / height >= 1 && event.getY() / height <= 5) {
			dragging = false;
			select((int) (event.getX() / width),
					(int) (event.getY() / height));
		}
		if(event.getY()/height > 5) {

			if(event.getX()/width >3 && input.length()>2) {
				onSubmitWord();
			}
			if(event.getX()/width <1){
				Music.playOnce(getContext(), R.raw.button);
				onCancelWord();
			}
		}
		return true;
		} else if(event.getAction() == MotionEvent.ACTION_MOVE){
			Log.d(TAG, "onDrag()"+event.getX()+","+event.getY());
			if(event.getY()/height>1&&event.getY()/height<5){
				dragging = true;
				if(inSmallerRect(event.getX(),event.getY())){
					Log.d(TAG,"select on drag"+(int) (event.getX() / width)+","+
							(int) (event.getY() / height));
				select((int) (event.getX() / width),
						(int) (event.getY() / height));
				}
			}
			return true;
		} else if(event.getAction() == MotionEvent.ACTION_UP
				&& event.getY()/height>1 && event.getY()/height<5
				&& dragging){
			if(input.length()>2) onSubmitWord();
			else onCancelWord();
			dragging = false;
			return true;
		} else {
			return super.onTouchEvent(event);
		}
	}

	private boolean inSmallerRect(float x, float y) {
		float xRatio = (float)(x/width)-(int)(x/width);
		float yRatio = (float)(y/height) - (int)(y/height);
		return yRatio>0.25&&yRatio<0.75&&xRatio>0.25&&xRatio<0.75;
	}

	private void backToBoggleMain() {
		Intent back = new Intent(boggleGame, Boggle.class);
		boggleGame.startActivity(back);		
	}

	private void onCancelWord() {
		Log.d(TAG,"Cancel!");
		//Close the path
		arrow.reset();
		//clear input
		input="";
		//clear selected tiles
		selectedTiles = new ArrayList<Integer>();
		//Redraw the canvas
		invalidate();
	}

	private void onSubmitWord() {
		Log.d(TAG, "Submit!"+"input len:"+input.length());
		//Close the path
		if(input.length()<3) return;
		arrow.reset();

		//Calculate score
		if(duplicatedWord()) {
			Music.playOnce(getContext(), R.raw.duplicate);
		}
		else if(isWord(input)){
			Music.playOnce(getContext(), R.raw.right);
			score += input.length() - 2;
			addToUsedWords();
			numWords++;
		}
		else {
			Music.playOnce(getContext(), R.raw.wrong);
		}

		//Clear input letters
		input = "";
		//Clear Selected Tiles
		selectedTiles = new ArrayList<Integer>();
		//Redraw the canvas
		invalidate();
	}

	private boolean inCircle(float x, float y, float cX, float cY, float radius) {
		return Math.sqrt((cX-x)*(cX-x)+(cY-y)*(cY-y))<=radius;
	}

	private void addToUsedWords() {
		usedWords.add(input);
	}

	private boolean duplicatedWord() {
		return usedWords.contains(input);
	}

	private void select(int x, int y) {
		invalidate();
		int selectX = Math.min(Math.max(x, 0), 3);
		int selectY = Math.min(Math.max(y, 1), 5) - 1;
		if(input==""){
			selX = selectX;
			selY =selectY;
		}else{
			if((Math.abs(selectX-selX) > 1 
					|| Math.abs(selectY - selY) >1
					|| (selectY == selY && selectX == selX))&&!dragging)
				return;
			else if(wasSelected(selectX,selectY)) 
				return;
			else{
				arrowP1.x = (int) (selX*width+width/2);
				arrowP1.y = (int) ((selY+1)*height+height/2);
				arrowP2.x = (int) (selX*width+width/2);
				arrowP2.y = (int) ((selY+1)*height+height/2);
				arrowP3.x = (int) (selectX*width+width/2);
				arrowP3.y = (int) ((selectY+1)*height+height/2);
				if(selY<selectY) {
					arrowP1.x+=width/10;
					arrowP2.x-=width/10;
				}
				if(selY>selectY){
					arrowP1.x-=width/10;
					arrowP2.x+=width/10;
				}
				if(selX<selectX) {
					arrowP1.y-=height/10;
					arrowP2.y+=height/10;
				}
				if(selX>selectX){
					arrowP1.y+=height/10;
					arrowP2.y-=height/10;
				}
				//invalidate();
				selX = selectX;
				selY =selectY;
			}
		}
		getRect(selX, selY, selRect);
		selectedTiles.add(selY*4+selX);
		Music.playOnce(getContext(), R.raw.select);
		invalidate();
		input += boggleGame.getTileString(selX, selY);
		Log.d(TAG, "input ="+input);
		Log.d(TAG, "selRect=" + selRect);
	}

	private boolean wasSelected(int x, int y){
		return selectedTiles.contains(4*y+x);
	}

	private boolean isWord(String word) {
		try {
			String fileName = findFile(word);
			Log.d(TAG, "File name is:"+fileName);
			int fileId = getResources().getIdentifier(fileName, "raw", getContext().getPackageName());
			if(fileId==0) {
				Log.d(TAG,"File Not Found.");
				return false;
			}
			InputStream wordList = getResources().openRawResource(fileId);
			BufferedReader reader = new BufferedReader(new InputStreamReader(wordList));
			String line;
			while((line = reader.readLine())!=null){
				if(line.equalsIgnoreCase(word)) {
					wordList.close();
					return true;
				}
			}
			wordList.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	private String findFile(String word) {
		String fileName = "wordlist"+ word.charAt(0)+word.charAt(1);
		fileName = fileName.toLowerCase();
		return fileName;
	}

}
