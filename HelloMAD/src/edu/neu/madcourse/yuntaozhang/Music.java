/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
 ***/
package edu.neu.madcourse.yuntaozhang;

import android.content.Context;
import android.media.MediaPlayer;
import edu.neu.madcourse.yuntaozhang.R;
import edu.neu.madcourse.yuntaozhang.boggle.BoggleSharedData;
import edu.neu.madcourse.yuntaozhang.goldcatcher.GCShared;
import edu.neu.madcourse.yuntaozhang.sudoku.SudokuPrefs;

public class Music {
	private static MediaPlayer mp = null;
	private static MediaPlayer mp1 = null;
	private static MediaPlayer mp2 = null;

	/** Stop old song and start new one */

	public static void playSudoku(Context context, int resource) {
		stop(context);

		// Start music only if not disabled in preferences
		if (SudokuPrefs.getMusic(context)) {
			mp = MediaPlayer.create(context, resource);
			mp.setLooping(true);
			mp.start();
		}
	}

	public static void playBoggle(Context context, int resource) {
		stop(context);
		if(BoggleSharedData.getBoggleMusicPref()) {
			mp = MediaPlayer.create(context, resource);
			mp.setLooping(true);
			mp.start();
		}
	}

	public static void playGoldCatcher(Context context, int resource) {
		stop(context);
		if(GCShared.music) {
			mp = MediaPlayer.create(context, resource);
			mp.setLooping(true);
			mp.start();
		}
	}

	public static void playOnce(Context context, int resource) {
		if(GCShared.music){
			if(mp1 != null){
				mp1.release();
				mp1 = null;
			}
			mp1 = MediaPlayer.create(context, resource);
			mp1.start();
		}
	}

	public static void outOfTime(Context context){
		if(GCShared.music){
			if(mp2 != null){
				mp2.release();
				mp2 = null;
			}
			mp2 = MediaPlayer.create(context, R.raw.outoftime);
			mp2.start();
		}
	}
	
	public static void stopOutOfTime(Context context){
		if(mp2 != null){
			mp2.stop();
			mp2.release();
			mp2 = null;
		}
	}


	/** Stop the music */
	public static void stop(Context context) { 
		if (mp != null) {
			mp.stop();
			mp.release();
			mp = null;
		}
	}
}
