package edu.neu.madcourse.yuntaozhang;

import edu.neu.madcourse.yuntaozhang.boggle.Boggle;
import edu.neu.madcourse.yuntaozhang.boggle.persistent.PersistentBoggle;
import edu.neu.madcourse.yuntaozhang.goldcatcher.GoldCatcher;
import edu.neu.madcourse.yuntaozhang.sudoku.Sudoku;
import edu.neu.mobileClass.*;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

public class HelloMAD extends Activity implements OnClickListener{
	private static final String TAG = "HelloMAD";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//this.setTitle(R.string.my_name);
		setContentView(R.layout.main_hello_mad);

		//PhoneCheckAPI.doAuthorization(this);

		// Set up click listeners for all the buttons
		View membersButton = findViewById(R.id.Team_Members_Button);
		membersButton.setOnClickListener(this);
		View sudokuButton = findViewById(R.id.Sudoku_Button);
		sudokuButton.setOnClickListener(this);
		View createErrorButton = findViewById(R.id.Create_error_Button);
		createErrorButton.setOnClickListener(this);
		View boggleButton = findViewById(R.id.Boggle_Button);
		boggleButton.setOnClickListener(this);
		View persistentBoggleButton = findViewById(R.id.Persistent_Boggle_Button);
		persistentBoggleButton.setOnClickListener(this);
		View exitButton = findViewById(R.id.Exit_Button);
		exitButton.setOnClickListener(this);
		View goldCatcherButton = findViewById(R.id.Gold_Catcher_Button);
		goldCatcherButton.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_hello_mad, menu);		
		return true;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.Team_Members_Button:
			Intent aboutMe = new Intent(this, AboutMe.class);
			startActivity(aboutMe);
			break;
		case R.id.Sudoku_Button:
			Intent sudoku = new Intent(this, Sudoku.class);
			startActivity(sudoku);
			break;
		case R.id.Boggle_Button:
			Intent boggle = new Intent(this, Boggle.class);
			startActivity(boggle);
			break;
		case R.id.Persistent_Boggle_Button:
			Intent persistentBoggle = new Intent(this, PersistentBoggle.class);
			startActivity(persistentBoggle);
			break;
		case R.id.Gold_Catcher_Button:
			Intent goldCatcher = new Intent(this, FinalProject.class);
			startActivity(goldCatcher);
			break;
		case R.id.Create_error_Button:
			Intent crash = new Intent(this, Crash.class);
			startActivity(crash);
			break;
		case R.id.Exit_Button:
			finish();
			break;
		}
	}
}
